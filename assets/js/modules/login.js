$('#_form_login').submit(function(e){
  e.preventDefault();

  $.ajax({
      url     : base_url + "c/authorize_user", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(oData){
        if(oData.status) {
          var template = "<div class='alert alert-success' style='margin-top: 10px;'>Redirecting</div>";
          $('#message').html(template);
          $(window).attr('location',oData.message)
        } else {
          var template = "<div class='alert alert-danger' style='margin-top: 10px;'>" + oData.message + "</div>";
          $('#message').html(template);
        }
      }
  });
});  

$('#_form_p_login').submit(function(e){
  e.preventDefault();

  $.ajax({
      url     : base_url + "c/authorize_personnel", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(oData){
        if(oData.status) {
          var template = "<div class='alert alert-success' style='margin-top: 10px;'>Redirecting</div>";
          $('#message').html(template);
          $(window).attr('location',oData.message)
        } else {
          var template = "<div class='alert alert-danger' style='margin-top: 10px;'>" + oData.message + "</div>";
          $('#message').html(template);
        }
      }
  });
});  


$('#_form_register').submit(function(e){
  e.preventDefault();

  $.ajax({
      url     : base_url + "c/register_customer", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(oData){
        alert('success');
        var template = "<div class='alert alert-success' style='margin-top: 10px;'>Redirecting</div>";
        $('#message').html(template);
        $(window).attr('location',oData.message)
      },
      error: function () {
        alert('Email address has already registered!');
      },
  });
});  

$('#province').change(function() {

  $.ajax({
      url     : base_url + "main/get_city_by_province_id", 
      type    : "post",
      data    : {
        id : $('#province').val()
      },
      dataType  : "JSON",
      success: function(oData){
        var data = oData.data;

        $('#city').empty();
        $('#city').append('<option disabled="" selected="">Select City</option>');

        if(data.length > 0) {

            for(var i in data){

                $('#city').append('<option value="' + data[i].id + '">' + data[i].city + '</option>');                            

            }

        }
      }
  });

});