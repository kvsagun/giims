function printData()
{
   var divToPrint=document.getElementById("printArea");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print').show();
}

$('.print').on('click',function(){
  $(this).hide();
  printData();
});

function printData2()
{
   var divToPrint=document.getElementById("printArea2");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print2').show();
}

$('.print2').on('click',function(){
  $(this).hide();
  printData2();
});

function printData3()
{
   var divToPrint=document.getElementById("printArea3");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print3').show();
}

$('.print3').on('click',function(){
  $(this).hide();
  printData3();
});

function printDataAll()
{
   var divToPrint=document.getElementById("printAreaAll");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print-all').show();
    $('.print').show();
    $('.print2').show();
    $('.print3').show();
}

$('.print-all').on('click',function(){
  $(this).hide();
  $('.print').hide();
  $('.print2').hide();
  $('.print3').hide();
  printDataAll();
});

var _year   = (new Date()).getFullYear();
var _year2  = (new Date()).getFullYear();
var _month  = 1;

$('#year').change(function() {
  _year = $(this).val();
  get_analytics_isp($(this).val());
});

$('#year2').change(function() {
  _year2 = $(this).val();
  get_analytics_validated_payment($(this).val(), _month);
});

$('#month').change(function() {
  _month = $(this).val();
  get_analytics_validated_payment(_year2, $(this).val());
});

get_analytics_isp(_year);
get_analytics_validated_payment(_year2, 1);
get_analytics_top_subscription();
get_analytics_total_active_users();
get_analytics_total_archive_users();
get_analytics_total_blocklist_users();
get_analytics_total_active_subscription();
get_analytics_total_disconnect_subscription();
get_analytics_total_terminate_subscription();

function get_analytics_validated_payment(year, month){
  $.ajax({
      type         : 'POST',
      url          : base_url + "main/get_analytics_validated_payment",
      data         : {
        year       : year,
        month      : month
      },
      datatype     : 'json',
      returnType   : 'json',
      beforeSend   : function () {
          $('.validated-table').empty();
      },
      success   : function(oData) {

          var data = oData.data;

          if(data.length > 0) {

              for(var i in data){

                  $('.validated-table').append('<tr><td><b>' + data[i].payment_date + '</b></td><td>' + data[i].full_name + '</td><td>' + data[i].isp + ' - ' + data[i].plan + '</td><td><b>&#8369;' + data[i].price + '</b></td></tr>');                            

              }

          } else if(data.length   == 0) {

              $('.validated-table').append('<div class="text-center">No Records Found.</div>');
              
          }

      }
    });
}

function get_analytics_isp(year){
  $('.isp-table-total').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_isp',
    data: {
      year : year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.isp-table-total').append('<tr><td><b>' + data[i].month_name + '</b></td><td>' + data[i].total + '</td><td><b>&#8369;' + data[i].sub_total + '</b></td></tr>');                            

          }

      } else if(data.length   == 0) {

          $('.isp-table-total').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_top_subscription(){
  $('.subscription-table-count').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_top_subscription',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.subscription-table-count').append('<tr><td>' + data[i].isp + ' - ' + data[i].plan + '</td><td>' + data[i].top_subscription + '</td></tr>');                            

          }

      } else if(data.length   == 0) {

          $('.subscription-table-count').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_total_active_users(){
  $('.active-user').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_total_active_users',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.active-user').append(data[i].total);                            

          }

      } else if(data.length   == 0) {

          $('.active-user').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_total_archive_users(){
  $('.archive-user').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_total_archive_users',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.archive-user').append(data[i].total);                            

          }

      } else if(data.length   == 0) {

          $('.archive-user').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_total_blocklist_users(){
  $('.blocklist-user').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_total_blocklist_users',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.blocklist-user').append(data[i].total);                            

          }

      } else if(data.length   == 0) {

          $('.blocklist-user').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_total_active_subscription(){
  $('.active-subscription').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_total_active_subscription',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.active-subscription').append(data[i].total);                            

          }

      } else if(data.length   == 0) {

          $('.active-subscription').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_total_disconnect_subscription(){
  $('.disconnect-subscription').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_total_disconnect_subscription',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.disconnect-subscription').append(data[i].total);                            

          }

      } else if(data.length   == 0) {

          $('.disconnect-subscription').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

function get_analytics_total_terminate_subscription(){
  $('.terminate-subscription').empty();
  $.ajax({
    type : 'POST',
    url : base_url + 'main/get_analytics_total_terminate_subscription',
    data: {
      year : _year
    },
    datatype: "json",
    returnType : 'json',
    success : function(oData){

      var data = oData.data;

      if(data.length > 0) {

          for(var i in data){

              $('.terminate-subscription').append(data[i].total);                            

          }

      } else if(data.length   == 0) {

          $('.terminate-subscription').append('<div class="text-center">No Records Found.</div>');
          
      }
    }
  });
}

