pagination_url = base_url + "main/get_customer_application_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_customer_application_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.subscription').html(data[i].isp + ' - ' + data[i].plan_name);
                uiListClone.find('.location').html(data[i].full_address);
                uiListClone.find('.contact-number').html(data[i].contact_number);
                uiListClone.find('.date-applied').html(data[i].date_applied);

                switch(data[i].status){
                  case "0":
                    uiListClone.find('.status').html('<h6><span class="badge badge-warning">New</span></h6>');
                    break;
                  case "1":
                    uiListClone.find('.status').html('<h6><span class="badge badge-success">Approved</span></h6>');
                    break;
                  case "2":
                    uiListClone.find('.status').html('<h6><span class="badge badge-danger">Rejected</span></h6>');
                    break;
                  default:
                    break;
                }


                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#action_type').val('update');
                $('#form-customer').val(oParentDetails.data('customer_name'));
                $('.form-date-applied').text(oParentDetails.data('date_applied'));
                $('.form-customer').text(oParentDetails.data('customer_name'));
                $('.form-isp').text(oParentDetails.data('isp'));
                $('.form-plan').text(oParentDetails.data('plan_name'));
                $('.form-remarks').text(oParentDetails.data('remarks'));
                $('.form-location').text(oParentDetails.data('full_address'));
                $('#createUpdateModal').modal('show');
            });    

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  
