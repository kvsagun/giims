pagination_url = base_url + "main/get_personnel_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_personnel_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.name').html(data[i].full_name);
                uiListClone.find('.email').html(data[i].email);
                uiListClone.find('.role').html(data[i].role);
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnEdit         = $('.table-list-cloned').find('.edit');                
            var btnDelete       = $('.table-list-cloned').find('.delete');   
                              
            btnEdit.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#action_type').val('update');
                $('#fname').val(oParentDetails.data('fname'));
                $('#lname').val(oParentDetails.data('lname'));
                $('#email').val(oParentDetails.data('email'));
                $('#password').val(oParentDetails.data('password'));
                $('#role').val(oParentDetails.data('role'));
                $('#createUpdateModal').modal('show');
            });      
  
            btnDelete.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');

                Swal.fire({
                  title: 'Are you sure to delete, ' + oParentDetails.data('name') + '?',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes!'
                }).then((result) => {
                  if (result.value) {

                    // delete
                    var data = {
                      id      : oParentDetails.data('id'),
                      action_type   : 'delete'
                    };

                    $.ajax({
                      url     : base_url + "main/save_personnel", 
                      type    : "post",
                      data    : data,
                      dataType  : "JSON",
                      success   : function(data){
                        $('#confirmModal').modal('hide');
                        load_table(iOffset, iLimit);
                        set_table_pagination(pagination_url);
                        Toast.fire({
                          type: 'success',
                          title: 'Successfully deleted'
                        });
                      }
                    });
                }
              })

            });

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/save_personnel", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
  });
});  