function printData()
{
   var divToPrint=document.getElementById("printArea");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print').show();
}

$('.print').on('click',function(){
  $(this).hide();
  printData();
});

pagination_url = base_url + "main/get_d_subscription_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_d_subscription_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.name').html(data[i].full_name);
                uiListClone.find('.email').html(data[i].email);
                uiListClone.find('.address').html(data[i].full_address);
                uiListClone.find('.subscription').html(data[i].isp + " - " + data[i].plan);
                uiListClone.find('.price').html('&#8369;' + data[i].subscription_price.toLocaleString('en-US', {minimumFractionDigits: 2}));
                var due_days_color = '';
                if(data[i].payment_due_days < 0){
                  due_days_color = 'text-danger';
                }else if(data[i].payment_due_days <= 7 && data[i].payment_due_days >= 0){
                  due_days_color = 'text-warning';
                }else{
                  due_days_color = 'text-success';
                }
                uiListClone.find('.due-days').html("<b class='" + due_days_color + "'>" + data[i].payment_due_days + '</b>');
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#customer_id').val(oParentDetails.data('customer_id'));
                $('#action_type').val('update');
                $('#form-customer').val(oParentDetails.data('full_name'));
                $('.form-date-applied').html(oParentDetails.data('date_approve'));
                $('.form-customer').html(oParentDetails.data('full_name'));
                $('.form-email').html(oParentDetails.data('email'));
                $('.form-location').html(oParentDetails.data('full_address'));
                $('.form-subscription').html(oParentDetails.data('isp') + " - " + oParentDetails.data('plan'));
                $('.form-price').html('&#8369;' + oParentDetails.data('subscription_price').toLocaleString('en-US', {minimumFractionDigits: 2}));
                $('.form-terms').html(oParentDetails.data('terms'));

                // get transaction to do
                // $.ajax({
                //     url     : base_url + "main/get_subscription_by_customer_id", 
                //     type    : "post",
                //     data    : {
                //       id : oParentDetails.data('id')
                //     },
                //     dataType  : "JSON",
                //     success: function(oData){
                //       var data = oData.data;
                //       var tmp = ''

                //       if(data.length > 0) {

                //           for(var i in data){

                //               var status = '';
                //               switch(data[i].status){
                //                 case '0' :
                //                   status = '<span class="text-primary">Active</span>';
                //                   break;
                //                 case '1' :
                //                   status = '<span class="text-warning">Disconnected</span>';
                //                   break;
                //                 case '2' :
                //                   status = '<span class="text-danger">Terminated</span>';
                //                   break;
                //                 default: 
                //                   break
                //                 }

                //               tmp = tmp + '<li>' + data[i].isp + ' - ' + data[i].plan + ' - &#8369;' + data[i].subscription_price + ' - ' + status + '</li>';


                //           }

                //       }
                //       $('.form-subscription').html(tmp);
                //     }
                // });

                $('#createUpdateModal').modal('show');
            });  

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/save_subscription", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
  });
});  

$('.reconnect').off('click').on('click',function(e) {

    e.preventDefault();

    Swal.fire({
        title: 'Are you sure to reconnect, ' + $('#form-customer').val() + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
          
          $.ajax({
              url     : base_url + "main/save_status_of_subscription", 
              type    : "post",
              data    : {
                id : $('#id').val(),
                action_type : 'reconnect',
                'customer_id' : $('#customer_id').val(),
              },
              dataType  : "JSON",
              success: function(data){
                $('#createUpdateModal').modal('hide');
                load_table(iOffset, iLimit);
                set_table_pagination(pagination_url);
                clear_form();
                Toast.fire({
                  type: 'success',
                  title: 'Successfully reconnect'
                });
              }
          });
      }
    })
  });  

$('.terminate').off('click').on('click',function(e) {

    e.preventDefault();

    Swal.fire({
        title: 'Are you sure to terminate, ' + $('#form-customer').val() + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
          
          $.ajax({
              url     : base_url + "main/save_status_of_subscription", 
              type    : "post",
              data    : {
                id : $('#id').val(),
                action_type : 'terminate',
                'customer_id' : $('#customer_id').val(),
              },
              dataType  : "JSON",
              success: function(data){
                $('#createUpdateModal').modal('hide');
                load_table(iOffset, iLimit);
                set_table_pagination(pagination_url);
                clear_form();
                Toast.fire({
                  type: 'success',
                  title: 'Successfully terminate'
                });
              }
          });
      }
    })
  }); 