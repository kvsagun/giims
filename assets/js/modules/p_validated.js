pagination_url = base_url + "main/get_validated_payment_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_validated_payment_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.payment-method').html(data[i].payment_method_name + ' - ' + data[i].payment_method_details);
                uiListClone.find('.name').html(data[i].full_name);
                uiListClone.find('.email').html(data[i].email);
                uiListClone.find('.address').html(data[i].full_address);
                uiListClone.find('.subscription').html(data[i].isp_name + " - " + data[i].plan_name + " - &#8369;" + data[i].price);
                uiListClone.find('.plan').html(data[i].plan);
                uiListClone.find('.price').html('&#8369;' + data[i].price);
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#action_type').val('update');
                $('#form-customer').val(oParentDetails.data('full_name'));
                $('#form_subscription_id').val(oParentDetails.data('subscription_id'));
                $('.form-date-applied').html(oParentDetails.data('date_applied'));
                $('.form-customer').html(oParentDetails.data('full_name'));
                $('.form-email').html(oParentDetails.data('email'));
                $('.form-location').html(oParentDetails.data('full_address'));
                $('.form-subscription').html(oParentDetails.data('isp_name') + " - " + oParentDetails.data('plan_name') + " - &#8369;" + oParentDetails.data('price'));
                $('.form-payment-method').html(oParentDetails.data('payment_method_name') + ' - ' + oParentDetails.data('payment_method_details'));
                $('.form-price').html('&#8369;' + oParentDetails.data('price'));
                $('.form-reference-number').html(oParentDetails.data('reference_number'));
                $('.form-payee-name').html(oParentDetails.data('payee_name'));

                $('#createUpdateModal').modal('show');
            });  

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/save_payment_subscription", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
  });
});  

$('.reject').off('click').on('click',function(e) {

    e.preventDefault();

    $('#createUpdateModal').modal('hide');
    Swal.fire({
        title: 'Are you sure to reject payment of, ' + $('#form-customer').val() + '?',
        type: 'warning',
        text: "Reject Payment Remarks:",
        input: 'text',
        inputValidator: (value) => {
          if (!value) {
            return 'You need to write something!'
          }
        },
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
          
          $.ajax({
              url     : base_url + "main/save_payment_rejection", 
              type    : "post",
              data    : {
                id : $('#id').val(),
                action_type : 'reject',
                'remarks' : result.value
              },
              dataType  : "JSON",
              success: function(data){
                load_table(iOffset, iLimit);
                set_table_pagination(pagination_url);
                clear_form();
                Toast.fire({
                  type: 'success',
                  title: 'Successfully reject'
                });
              }
          });
      }
    })
  }); 