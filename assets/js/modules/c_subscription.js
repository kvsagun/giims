function printData()
{
   var divToPrint=document.getElementById("printArea");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print').show();
}

$('.print').on('click',function(){
  $(this).hide();
  printData();
});

pagination_url = base_url + "main/get_subscription_pagination_by_customer_id";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_subscription_by_customer_id_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.date-applied').html(data[i].date_approve);
                uiListClone.find('.subscription').html(data[i].isp + " - " + data[i].plan);
                uiListClone.find('.location').html(data[i].full_address);
                uiListClone.find('.price').html('&#8369;' + data[i].subscription_price.toLocaleString('en-US', {minimumFractionDigits: 2}));
                switch(data[i].status){
                  case "0":
                    uiListClone.find('.status').html('<h6><span class="badge badge-success">Active</span></h6>');
                    break;
                  case "1":
                    uiListClone.find('.status').html('<h6><span class="badge badge-warning">Disconnected</span></h6>');
                    break;
                  case "2":
                    uiListClone.find('.status').html('<h6><span class="badge badge-danger">Terminated</span></h6>');
                    break;
                  default:
                    break;
                }
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#customer_id').val(oParentDetails.data('customer_id'));
                $('#action_type').val('update');
                $('#form-customer').val(oParentDetails.data('full_name'));
                $('.form-date-applied').html(oParentDetails.data('date_approve'));
                $('.form-customer').html(oParentDetails.data('full_name'));
                $('.form-email').html(oParentDetails.data('email'));
                $('.form-location').html(oParentDetails.data('full_address'));
                $('.form-subscription').html(oParentDetails.data('isp') + " - " + oParentDetails.data('plan'));
                $('.form-terms').html(oParentDetails.data('terms'));

                $('#createUpdateModal').modal('show');
            });  

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);