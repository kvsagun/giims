function printData()
{
   var divToPrint=document.getElementById("printArea");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('.print').show();
}

$('.print').on('click',function(){
  $(this).hide();
  printData();
})

pagination_url = base_url + "main/get_subscription_payment_by_customer_id_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  load_due();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_customer_subscription_payment",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.payment-date').html(data[i].payment_date);
                uiListClone.find('.subscription').html(data[i].isp_name + ' - ' + data[i].plan_name);
                uiListClone.find('.isp').html(data[i].isp);
                uiListClone.find('.price').html('&#8369;' + data[i].price);
                uiListClone.find('.location').html(data[i].full_address);
                uiListClone.find('.date-applied').html(data[i].date_applied);

                switch(data[i].status){
                  case "0":
                    uiListClone.find('.status').html('<h6><span class="badge badge-warning">For Validation</span></h6>');
                    break;
                  case "1":
                    uiListClone.find('.status').html('<h6><span class="badge badge-success">Validated (Paid)</span></h6>');
                    break;
                  case "2":
                    uiListClone.find('.status').html('<h6><span class="badge badge-danger">Rejected</span></h6>');
                    break;
                  default:
                    break;
                }


                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('.form-date-applied').html(oParentDetails.data('date_applied'));
                $('.form-customer').html(oParentDetails.data('full_name'));
                $('.form-email').html(oParentDetails.data('email'));
                $('.form-location').html(oParentDetails.data('full_address'));
                $('.form-subscription').html(oParentDetails.data('isp_name') + " - " + oParentDetails.data('plan_name') + " - &#8369;" + oParentDetails.data('price'));
                $('.form-payment-method').html(oParentDetails.data('payment_method_name') + ' - ' + oParentDetails.data('payment_method_details'));
                $('.form-price').html('&#8369;' + oParentDetails.data('price'));
                $('.form-reference-number').html(oParentDetails.data('reference_number'));
                $('.form-payee-name').html(oParentDetails.data('payee_name'));
                $('.form-remarks').html(oParentDetails.data('remarks'));
                $('.print').show();
                $('#viewDetails').modal('show');
            });    

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);
load_all_subs();

function load_due() {

  var data = {
      id  : $('#customer_id').val()
  };

  $('#subscription_id').empty();

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/get_customer_due_subscription",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;
        var due = '';

        if(data.length > 0) {

            $('#subscription_id').append('<option value="all" selected="">All</option>');

            for(var i in data){
                            
                due += '<option value="' + data[i].id + '">' + data[i].isp + ' - ' + data[i].plan + ' - &#8369;'  + data[i].subscription_price.toLocaleString('en-US', {minimumFractionDigits: 2}) + ' (' + data[i].next_payment + ')</option>';

            }  

            $('#subscription_id').append(due);                            

        } else if(data.length   == 0) {

            $('#subscription_id').append('<option value="" Selected="true" disabled>No Available Subscriptions</option>');
            
        }

    }
  });
}

function load_all_subs() {

  var data = {
      id  : $('#customer_id').val()
  };

  $('.plan-name').empty();

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/get_customer_all_subscription",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;
        var subs = '';

        if(data.length > 0) {

            for(var i in data){
                            
                subs += '<li>' + data[i].isp + ' - ' + data[i].plan + ' - &#8369;'  + data[i].subscription_price.toLocaleString('en-US', {minimumFractionDigits: 2}) + ' (' + data[i].next_payment + ')</li>';

            }  

            $('.plan-name').append(subs);                            

        } else if(data.length   == 0) {

            $('.plan-name').append('<li>No Available Subscriptions</li>');
            
        }

    }
  });
}

load_payment_details();

function load_payment_details() {

  var data = {
      id  : $('#customer_id').val()
  };

  $('#payment_method').empty();

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/get_payment_method",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;
        var p_method = '';

        if(data.length > 0) {
            $('#payment_method').append('<option value="" Selected="true" disabled>Select Payment Method</option>');

            for(var i in data){
                            
                p_method += '<option value="' + data[i].id + '" details="' + data[i].details + '">' + data[i].name + '</option>';

            }  

            $('#payment_method').append(p_method);                            



            $("#payment_method").change(function(){ 
                var element = $(this).find('option:selected'); 
                var myTag = element.attr("details"); 

                $('.form-payment-details').html(myTag); 
            }); 

        } else if(data.length   == 0) {

            $('#payment_method').append('<option value="" Selected="true" disabled>No Available Payment Method</option>');
            
        }

    }
  });
}

function set_label_all_subs() {

  var data = {
      id                : $('#customer_id').val()
  };

  $('.form-subs').empty();

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/get_customer_due_subscription",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;
        var subs = '';

        var total_price = 0;

        if(data.length > 0) {

            for(var i in data){


                subs += '<li>' + data[i].isp + ' - ' + data[i].plan + ' - &#8369;'  + data[i].subscription_price + ' (' + data[i].full_address + ')</li>';

                total_price = parseFloat(total_price) + parseFloat(data[i].subscription_price);

            }  
                $('.form-subs').html(subs);

                $('.total-price').html('&#8369;' + total_price.toLocaleString('en-US', {minimumFractionDigits: 2}));
                $('.balance').html('&#8369;' + total_price.toLocaleString('en-US', {minimumFractionDigits: 2}));

            // $('.plan-name').append(subs);                            

        } else if(data.length   == 0) {

            $('.form-subs').html('<li>No Available Subscriptions</li>');
            
        }

    }
  });
}

function set_label_selected_subs() {

  var data = {
      id  : $('#customer_id').val(),
      subscription_id   : $('#subscription_id').val()
  };
  
  $('.form-subs').empty();

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/get_customer_due_subscription",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;
        var subs = '';

        var total_price = 0;

        if(data.length > 0) {

            for(var i in data){


                subs += '<li>' + data[i].isp + ' - ' + data[i].plan + ' - &#8369;'  + data[i].subscription_price + ' (' + data[i].full_address + ')</li>';

                total_price = parseFloat(total_price) + parseFloat(data[i].subscription_price);

            }  
                $('.form-subs').html(subs);

                $('.total-price').html('&#8369;' + total_price.toLocaleString('en-US', {minimumFractionDigits: 2}));
                $('#price').val(total_price.toLocaleString('en-US', {minimumFractionDigits: 2}));

            // $('.plan-name').append(subs);                            

        } else if(data.length   == 0) {

            $('.form-subs').html('<li>No Available Subscriptions</li>');
            
        }

    }
  });
}

set_label_all_subs();

$('#subscription_id').change(function() {
  if($('#subscription_id').val() == 'all'){
    set_label_all_subs();
  }else{
    set_label_selected_subs();
  }
});

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/save_subscription_payment", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
  });
});  
