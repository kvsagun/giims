pagination_url = base_url + "main/get_b_customer_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_b_customer_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.name').html(data[i].full_name);
                uiListClone.find('.email').html(data[i].email);
                uiListClone.find('.address').html(data[i].full_address);
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#action_type').val('update');
                $('#form-customer').val(oParentDetails.data('full_name'));
                $('.form-date-applied').text(oParentDetails.data('date_applied'));
                $('.form-customer').text(oParentDetails.data('full_name'));
                $('.form-email').text(oParentDetails.data('email'));
                $('.form-location').text(oParentDetails.data('full_address'));

                var total_price = 0;

                $.ajax({
                    url     : base_url + "main/get_customer_due_subscription", 
                    type    : "post",
                    data    : {
                      id : oParentDetails.data('id')
                    },
                    dataType  : "JSON",
                    success: function(oData){
                      var data = oData.data;
                      var tmp = ''

                      if(data.length > 0) {

                          for(var i in data){

                              var status = '';
                              switch(data[i].status){
                                case '0' :
                                  status = '<span class="text-primary">Active</span>';
                                  break;
                                case '1' :
                                  status = '<span class="text-warning">Disconnected</span>';
                                  break;
                                case '2' :
                                  status = '<span class="text-danger">Terminated</span>';
                                  break;
                                default: 
                                  break
                                }

                              tmp = tmp + '<li>' + data[i].isp + ' - ' + data[i].plan + ' - &#8369;' + data[i].subscription_price + ' - ' + status + '</li>';
                              total_price = parseFloat(total_price) + parseFloat(data[i].subscription_price);

                          }

                      }
                      $('.form-balance').html('&#8369;' + total_price.toLocaleString('en-US', {minimumFractionDigits: 2}));
                      $('.form-subscription').html(tmp);
                    }
                });
                
                $('#createUpdateModal').modal('show');
            });  

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/save_customer", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
  });
});  

$('.remove-blocklist').off('click').on('click',function(e) {

    e.preventDefault();

    Swal.fire({
        title: 'Are you sure to remove from blocklist, ' + $('#form-customer').val() + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {

          var data = {
            id            : oParentDetails.data('id')
          };
          
          $.ajax({
              url     : base_url + "main/save_status_of_customer", 
              type    : "post",
              data    : {
                id : $('#id').val(),
                action_type : 'remove-blocklist'
              },
              dataType  : "JSON",
              success: function(data){
                $('#createUpdateModal').modal('hide');
                load_table(iOffset, iLimit);
                set_table_pagination(pagination_url);
                clear_form();
                Toast.fire({
                  type: 'success',
                  title: 'Successfully remove from blocklist'
                });
              }
          });
      }
    })
  });  

$('.archive').off('click').on('click',function(e) {

    e.preventDefault();

    Swal.fire({
        title: 'Are you sure to archive, ' + $('#form-customer').val() + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {

          var data = {
            id            : oParentDetails.data('id'),
            action_type   : 'archive'
          };
          
          $.ajax({
              url     : base_url + "main/save_status_of_customer", 
              type    : "post",
              data    : {
                id : $('#id').val(),
                action_type : 'archive'
              },
              dataType  : "JSON",
              success: function(data){
                $('#createUpdateModal').modal('hide');
                load_table(iOffset, iLimit);
                set_table_pagination(pagination_url);
                clear_form();
                Toast.fire({
                  type: 'success',
                  title: 'Successfully archive'
                });
              }
          });
      }
    })
  }); 