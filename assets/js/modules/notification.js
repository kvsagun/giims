pagination_url = base_url + "main/get_notification_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

$('.btn-add').off('click').on('click', function() {
  clear_form();
  $('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset  : offset,
      limit   : limit,
      search  : search
  };

  $.ajax({
    type         : 'POST',
    url          : base_url + "main/load_notification_table",
    data         : data,
    datatype     : 'json',
    returnType   : 'json',
    beforeSend   : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success   : function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.type').html(data[i].type);
                uiListClone.find('.details').html(data[i].details);

                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

            }
  
            var btnView = $('.table-list-cloned').find('.view');      
                              
            btnView.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#action_type').val('update');
                $('#form-customer').val(oParentDetails.data('customer_name'));
                $('#customer_id').val(oParentDetails.data('customer_id'));
                $('.form-type').text(oParentDetails.data('type'));
                $('.form-details').text(oParentDetails.data('details'));
                $('.form-link').attr('href', oParentDetails.data('redirrect_link'));

                $.ajax({
                    url     : base_url + "main/read_notification", 
                    type    : "post",
                    data    : {
                      id    : $('#id').val()
                    },
                    dataType  : "JSON",
                    success: function(data){
                      load_table(iOffset, iLimit);
                      set_table_pagination(pagination_url);

                      $.ajax({
                          url     : base_url + "main/get_number_of_notifications", 
                          type    : "post",
                          data    : {
                            id    : $('#id').val()
                          },
                          dataType  : "JSON",
                          success: function(data){
                            var data = data.data;
                            if(data[0].number_of_notifications == 0){
                              $('.num-notification').html('');
                              $('.num-notification').removeClass('badge badge-pill badge-danger ml-2');
                            }else{
                              $('.num-notification').html(data[0].number_of_notifications);
                            }
                          }
                      });
                    }
                });

                $('#createUpdateModal').modal('show');
            });    

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/approved_application", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
  });
});  
