function clear_form(){
  $('#action_type').val('create');
  $('#id').val('');
  $('#name').val('');
}

function step1(){
  $('.step-1').show();
  $('.step-2').hide();
}

function step2(isp_id, name){
  if($('#customer_id').val()){
  $('.step-1').hide();
  $('.form-isp').html(name);
  $.ajax({
      url     : base_url + "main/get_plan_by_isp_id", 
      type    : "post",
      data    : {
        'id' : isp_id
      },
      dataType  : "JSON",
      beforeSend   : function () {
          uiListContainter.empty();
      },
      success: function(oData){
        console.log(oData);
        var data = oData.data;
        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.plan-name').html(data[i].name);
                uiListClone.find('.price').html('&#8369;' + data[i].price);
                uiListClone.find('.description').html(data[i].description);
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                uiListContainter.append(uiListClone);                            

  
                var btnSelectPlan = $('.table-list-cloned').find('.selected-plan');   

                btnSelectPlan.off('click').on('click',function() {
                    if($('#customer_id').val()){
                      oParentDetails  = $(this).closest('.table-list-cloned');
                      $('#isp').val(oParentDetails.data('isp_id'));
                      $('#plan').val(oParentDetails.data('id'));
                      $('#total_price').val(oParentDetails.data('price'));
                      $('.form-plan').html(oParentDetails.data('name'));
                      $('.form-terms').attr('href', base_url + "h/terms/" + oParentDetails.data('id'));
                      $('.form-price').html('&#8369;' + oParentDetails.data('price'));
                      $('#createUpdateModal').modal('show');                      
                    }else{
                      $(window).attr('location',base_url + "c")
                    }
                });      
            }

        } else if(data.length   == 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }
        clear_form();
        $('.step-2').show();
      }
  });
  }else{
    $(window).attr('location',base_url + "c")
  }
}

/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
  var id = $('#id').val();

  e.preventDefault();

  $.ajax({
      url     : base_url + "main/save_application", 
      type    : "post",
      data    : $(this).serializeArray(),
      dataType  : "JSON",
      success: function(data){
        $('#createUpdateModal').modal('hide');
        clear_form();
        alert("Internet Application Successfully!");
        $(window).attr('location',base_url + "h/application")
      }
  });
});  

$("#terms").change(function() {
    if($(this).prop('checked')) {
        $('.approve').removeAttr("disabled");
    } else {
        $(".approve").attr("disabled", true);
    }
});
