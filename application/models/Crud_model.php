<?php

class Crud_model extends CI_MODEL{

	// ISP	

	function add_isp($data)
	{
		$sql = "INSERT INTO isp(
					name)
				VALUES(
				'".$data['name']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_isp($data)
	{
		$sql = "UPDATE isp
				SET name 				= '".$data['name']."'
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_isp($data)
	{
		$sql = "UPDATE isp
				SET is_active = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_isp_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `isp`
				WHERE is_active = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_isp($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
				  *
				FROM
				  isp
				WHERE is_active = 0 " . $where_query . " " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	// PLAN

	function add_plan($data)
	{
		$sql = "INSERT INTO plan(
					isp_id, 
					name, 
					description, 
					price, 
					terms)
				VALUES(
				'".$data['isp']."', 
				'".$data['plan_name']."', 
				'".$data['description']."', 
				'".$data['price']."', 
				'".$data['terms']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_plan($data)
	{
		$sql = "UPDATE plan
				SET isp_id 				= '".$data['isp']."',
					name 				= '".$data['plan_name']."',
					description 		= '".$data['description']."',
					price 				= '".$data['price']."',
					terms 				= '".$data['terms']."'
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_plan($data)
	{
		$sql = "UPDATE plan
				SET is_active = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_plan_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM plan p
				INNER JOIN isp i
				on i.id = p.isp_id
				WHERE p.is_active = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_plan($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND p.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
				  p.id,
				  p.isp_id as isp_id,
				  i.name as isp,
				  p.name,
				  p.price,
				  p.description,
				  p.terms
				FROM
				  plan p
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE p.is_active = 0 " . $where_query . "
				ORDER BY p.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_plan_by_isp_id($id = 0)
	{
		$sql = "SELECT
				  p.id,
				  p.isp_id as isp_id,
				  i.name as isp,
				  p.name,
				  p.price,
				  p.description
				FROM
				  plan p
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE p.is_active = 0 
				AND i.id = " . $id . "
				ORDER BY p.date_created ASC";

        $result = $this->db->query($sql);
        return $result->result_array();
	}


	// APPLICATION

	function save_application($data)
	{
		$sql = "INSERT INTO application(
					plan_id, 
					customer_id, 
					line1,
					line2,
					province,
					city,
					zipcode,
					total_price,
					contact_number)
				VALUES(
				'".$data['plan']."', 
				'".$data['customer_id']."', 
				'".$data['line1']."',
				'".$data['line2']."',
				'".$data['province']."',
				'".$data['city']."',
				'".$data['zipcode']."',
				'".$data['total_price']."',
				'".$data['contact_number']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_application($data)
	{
		$sql = "UPDATE application
				SET plan_id 			= '".$data['plan']."',
					location 			= '".$data['location']."',
					price 				= '".$data['price']."'
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function approved_application($data)
	{
		$sql = "UPDATE application
				SET status = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function save_subscription($data)
	{
		$sql = "INSERT INTO subscription(
					plan_id, 
					customer_id, 
					subscription_price,
					next_payment)
				VALUES(
				'".$data['plan_id']."', 
				'".$data['customer_id']."', 
				'".$data['subscription_price']."',
				CASE
					WHEN 
						DAY(NOW()) <= 21 THEN DATE_ADD(NOW(), INTERVAL 1 MONTH)
					ELSE
						DATE_ADD(DATE_ADD(NOW(), INTERVAL 1 MONTH), INTERVAL 10 DAY)
				END
			)";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function reject_application($data)
	{
		$sql = "UPDATE application
				SET status = 2,
					remarks = '".$data['remarks']."'
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_application_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
				    OR
				    a.location LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `application` a
				INNER JOIN `plan` p
				ON p. id = a.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN `customer` c
				ON c.id = a.customer_id
				WHERE 0 = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_customer_application_pagination($limit = 10, $search = '', $customer_id){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
				    OR
				    CONCAT(a.line1, ' ', a.line2, ' ', ct.name, ' ', pr.name, ' ', a.zipcode) LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `application` a
				INNER JOIN `plan` p
				ON p. id = a.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN `customer` c
				ON c.id = a.customer_id
				INNER JOIN cities ct
				on ct.id = a.city
				INNER JOIN provinces pr
				on pr.id = a.province
				WHERE a.customer_id = " . $customer_id;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_application($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND a.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
				    OR
				    CONCAT(a.line1, ' ', a.line2, ' ', ct.name, ' ', pr.name, ' ', a.zipcode) LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					a.id,
					a.plan_id,
					a.customer_id,
					a.total_price,
					a.contact_number,
					c.email,
					CONCAT(a.line1, ' ', a.line2, ' ', ct.name, ' ', pr.name, ' ', a.zipcode) AS full_address,
					a.status,
					IFNULL(a.remarks, '') AS remarks,
					CONCAT(c.lname, ', ', c.fname) AS customer_name,
					p.name AS plan_name,
					i.name AS isp,
					DATE_FORMAT(a.`date_created`, '%M %d, %Y') as date_applied
				FROM `application` a
				INNER JOIN `plan` p
				ON p. id = a.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN `customer` c
				ON c.id = a.customer_id
				INNER JOIN cities ct
				on ct.id = a.city
				INNER JOIN provinces pr
				on pr.id = a.province
				WHERE 0 = 0 " . $where_query . "
				ORDER BY a.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_application_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND a.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
				    OR
					CONCAT(a.line1, ' ', a.line2, ' ', ct.name, ' ', p.name, ' ', a.zipcode) AS full_address,
				    OR
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					a.id,
					a.plan_id,
					a.customer_id,
					a.contact_number,
					c.email,
					CONCAT(a.line1, ' ', a.line2, ' ', ct.name, ' ', pr.name, ' ', a.zipcode) AS full_address,
					a.status,
					IFNULL(a.remarks, '') AS remarks,
					CONCAT(c.lname, ', ', c.fname) AS customer_name,
					p.name AS plan_name,
					i.name AS isp,
					DATE_FORMAT(a.`date_created`, '%M %d, %Y') as date_applied
				FROM `application` a
				INNER JOIN `plan` p
				ON p. id = a.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN `customer` c
				ON c.id = a.customer_id
				INNER JOIN cities ct
				on ct.id = a.city
				INNER JOIN provinces pr
				on pr.id = a.province
				WHERE 0 = 0 " . $where_query . "
				ORDER BY a.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}



	// login/registration

	function register_customer($data)
	{
		$sql = "INSERT INTO customer(
					fname, 
					mname, 
					lname, 
					email, 
					password,
					address_line1,
					address_line2,
					city,
					province,
					zipcode)
				VALUES(
				'".$data['first_name']."', 
				'".$data['middle_name']."', 
				'".$data['last_name']."', 
				'".$data['email']."', 
				'".$data['password']."',
				'".$data['address_line1']."',
				'".$data['address_line2']."',
				'".$data['city']."',
				'".$data['province']."',
				'".$data['zipcode']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function authorize_user($data)
	{
		 $sql = "SELECT
				  *
				FROM
				  customer u
				 WHERE u.email = '" . $data['email'] . "'
				 AND u.password = '" . $data['password'] . "'";

        $result = $this->db->query($sql);
        return $result->row_array();
	}

	function authorize_personnel($data)
	{
		 $sql = "SELECT
				  u.id, 
				  u.`fname`, 
				  u.`lname`, 
				  u.`email`, 
				  u.is_active,
				  u.role
				FROM
				  user u
				 WHERE u.email = '" . $data['email'] . "'
				 AND u.password = '" . $data['password'] . "'";

        $result = $this->db->query($sql);
        return $result->row_array();
	}

	function save_status_of_customer($data, $status)
	{
		$sql = "UPDATE customer
				SET is_active			= '" . $status . "'
				WHERE id 				= '" . $data['id'] . "'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_customer($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND c.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) LIKE '%" . $search . "%')";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					c.id,
					c.fname,
					c.lname,
					c.email,
					c.password,
					c.is_active,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) AS full_address
				FROM `customer` c
				INNER JOIN provinces p
				ON p.id = c.province
				INNER JOIN cities ct
				ON ct.id = c.city
				WHERE c.is_active = 0 " . $where_query . "
				ORDER BY c.id DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_customer_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) LIKE '%" . $search . "%')";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM customer c
				WHERE c.is_active = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_b_customer($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND c.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) LIKE '%" . $search . "%')";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					c.id,
					c.fname,
					c.lname,
					c.email,
					c.password,
					c.is_active,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) AS full_address
				FROM `customer` c
				INNER JOIN provinces p
				ON p.id = c.province
				INNER JOIN cities ct
				ON ct.id = c.city
				WHERE c.is_active = 1 " . $where_query . "
				ORDER BY c.id DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_b_customer_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) LIKE '%" . $search . "%')";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM customer c
				WHERE c.is_active = 1 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_arc_customer($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND c.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) LIKE '%" . $search . "%')";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					c.id,
					c.fname,
					c.lname,
					c.email,
					c.password,
					c.is_active,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) AS full_address
				FROM `customer` c
				INNER JOIN provinces p
				ON p.id = c.province
				INNER JOIN cities ct
				ON ct.id = c.city
				WHERE c.is_active = 2 " . $where_query . "
				ORDER BY c.id DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_arc_customer_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', p.name, ' ', c.zipcode) LIKE '%" . $search . "%')";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM customer c
				WHERE c.is_active = 2 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_personnel_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    c.email LIKE '%" . $search . "%')";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM customer c
				WHERE c.is_active = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_personnel($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND p.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(u.lname, ', ', u.fname) LIKE '%" . $search . "%'
				    OR
				    u.role LIKE '%" . $search . "%'
				    OR
				    u.email LIKE '%" . $search . "%')";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					u.id,
					u.fname,
					u.lname,
					u.email,
					u.password,
					u.is_active,
					u.role,
					CONCAT(u.lname, ', ', u.fname) AS full_name
				FROM `user` u
				WHERE u.is_active = 0 " . $where_query . "
				ORDER BY u.id DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function add_personnel($data)
	{
		$sql = "INSERT INTO user(
					fname, 
					lname, 
					email, 
					password, 
					role)
				VALUES(
				'".$data['fname']."', 
				'".$data['lname']."', 
				'".$data['email']."', 
				'".$data['password']."', 
				'".$data['role']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_personnel($data)
	{
		$sql = "UPDATE user
				SET fname 				= '".$data['fname']."',
					lname 				= '".$data['lname']."',
					email 				= '".$data['email']."',
					password 			= '".$data['password']."',
					role 				= '".$data['role']."'
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_personnel($data)
	{
		$sql = "UPDATE user
				SET is_active = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_provinces()
	{
			$sql = "SELECT
					*
				FROM provinces";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_city_by_province_id($id = 0)
	{
		$where_query = '';

		if($id != 0){
			$where_query .= " AND c.province_id = " . $id;
		}

		$sql = "SELECT 
					c.id,
					p.name AS province,
					c.name AS city
				FROM `cities` c
				INNER JOIN `provinces` p
				ON p.id = c.province_id
				WHERE 0 = 0 " . $where_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}


	// SUBSCRIPTION

	function update_asubscription($data)
	{
		$sql = "UPDATE subscription
				SET plan_id 			= '".$data['plan']."',
					location 			= '".$data['location']."',
					price 				= '".$data['price']."'
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function disconnect_subscription($data)
	{
		$sql = "UPDATE subscription
				SET status = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function terminite_subscription($data)
	{
		$sql = "UPDATE subscription
				SET status = 2
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function reconnect_subscription($data)
	{
		$sql = "UPDATE subscription
				SET status = 0
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_a_subscription_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.status = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_a_subscription_pagination_by_customer_id($limit = 10, $search = '', $customer_id){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.status = 0 
				AND s.customer_id = " . $customer_id;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_a_subscription($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE s.status = 0 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_a_subscription_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE s.status = 0 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_ad_subscription_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE (s.status = 0 OR s.status = 1) " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_d_subscription_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.status = 1 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_d_subscription_pagination_by_customer_id($limit = 10, $search = '', $customer_id){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.status = 1 
				AND s.customer_id = " . $customer_id;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_d_subscription($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE s.status = 1 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_d_subscription_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE s.status = 1 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_t_subscription_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.status = 2 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_t_subscription_pagination_by_customer_id($limit = 10, $search = '', $customer_id){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.status = 2 
				AND s.customer_id = " . $customer_id;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_t_subscription($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) AS full_address
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE s.status = 2 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_t_subscription_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					p.terms,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE s.status = 2 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_subscription_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname) LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					i.name AS isp,
					p.terms,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, now()) as payment_due_days,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE 0 = 0 " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_subscription_pagination_by_customer_id($id = 0, $limit = 10, $search = ''){

		$where_query = "";

		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
					CONCAT(c.lname, ', ', c.fname)  LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(s.`last_payment`, '%M %d, %Y') LIKE '%" . $search . "%'
				    OR
				    CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE 0 = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_customer_due_subscription($id = 0, $s_id = 0)
	{
		$where_query = '';
		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		if($s_id != 0){
			$where_query .= " AND s.id = " . $s_id;
		}

		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, NOW()) AS payment_due_days,
					DATE_FORMAT(s.`next_payment`, '%M %d, %Y') AS next_payment,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE (s.status = 0 or s.status = 1) 
				AND (DATEDIFF(s.next_payment, NOW()) < 31) " . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_customer_all_subscription($id = 0)
	{
		$where_query = '';
		if($id != 0){
			$where_query .= " AND s.customer_id = " . $id;
		}

		$sql = "SELECT
					s.id,
					s.plan_id,
					s.customer_id,
					c.email,
					DATE_FORMAT(s.`date_created`, '%M %d, %Y') AS date_approve,
					s.subscription_price,
					IF(ISNULL(s.`last_payment`), '', DATE_FORMAT(s.`last_payment`, '%M %d, %Y')) AS last_payment,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					p.name AS plan,
					i.name AS isp,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					DATEDIFF(s.next_payment, NOW()) AS payment_due_days,
					DATE_FORMAT(s.`next_payment`, '%M %d, %Y') AS next_payment,
					s.status
				FROM `subscription` s
				INNER JOIN customer c
				ON c.id = s.customer_id
				INNER JOIN plan p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				WHERE (s.status = 0 or s.status = 1)" . $where_query . "
				ORDER BY payment_due_days, s.date_created DESC";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function subscription_payment_all($data)
	{
		$sql = "SELECT 
					* 
				FROM subscription s
				WHERE s.status = 0 or s.status = 1
				AND s.customer_id = '".$data['customer_id']."'
				AND (DATEDIFF(s.next_payment, NOW()) < 31)";

        $res = $this->db->query($sql);

        if($res->num_rows() > 0)
		{
			foreach($res->result() as $rows)
			{

				$sql2 = "INSERT INTO subscription_payment(
							customer_id, 
							subscription_id,
							reference_number,
							payee_name,
							price,
							payment_method)
						VALUES(
						'".$data['customer_id']."', 
						'".$rows->id."',
						'".$data['reference_number']."',
						'".$data['payee_name']."',
						'".$rows->subscription_price."',
						'".$data['payment_method']."')";

		        $this->db->query($sql2);

				$result[] = array(
					$rows->id
					);
			}
		}

        return $result;
	}

	function subscription_payment($data)
	{
		$sql = "INSERT INTO subscription_payment(
					customer_id, 
					subscription_id,
					reference_number,
					payee_name,
					price,
					payment_method)
				VALUES(
				'".$data['customer_id']."', 
				'".$data['subscription_id']."',
				'".$data['reference_number']."',
				'".$data['payee_name']."',
				'".$data['price']."',
				'".$data['payment_method']."')";

        $this->db->query($sql);

        return $this->db->insert_id();
	}

	function get_subscription_payment_by_customer_id($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND sp.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					sp.id,
					sp.customer_id,
					sp.reference_number,
					sp.payee_name,
					sp.remarks,
					sp.price,
					p.name AS plan_name,
					i.name AS isp_name,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					c.email,
					DATE_FORMAT(sp.`date_created`, '%M %d, %Y') AS payment_date,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					pm.name as payment_method_name,
					pm.details as payment_method_details,
					sp.status
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE 0 = 0 " . $where_query . "
				ORDER BY sp.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_subscription_payment_by_customer_id_pagination($id = 0, $limit = 10, $search = ''){

		$where_query = "";

		if($id != 0){
			$where_query .= " AND sp.customer_id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE 0 = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_payment_validation($limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					sp.id,
					sp.customer_id,
					sp.reference_number,
					sp.payee_name,
					sp.remarks,
					sp.price,
					p.name AS plan_name,
					i.name AS isp_name,
					sp.subscription_id,
					DATE_FORMAT(sp.`date_created`, '%M %d, %Y') AS payment_date,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					pm.name as payment_method_name,
					pm.details as payment_method_details,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					c.email,
					sp.status
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 0 " . $where_query . "
				ORDER BY sp.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_payment_validation_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_validated_payment($limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					sp.id,
					sp.customer_id,
					sp.reference_number,
					sp.payee_name,
					sp.remarks,
					sp.price,
					p.name AS plan_name,
					i.name AS isp_name,
					sp.subscription_id,
					DATE_FORMAT(sp.`date_created`, '%M %d, %Y') AS payment_date,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					pm.name as payment_method_name,
					pm.details as payment_method_details,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					c.email,
					sp.status
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 1 " . $where_query . "
				ORDER BY sp.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_validated_payment_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 1 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_p_reject($limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
					sp.id,
					sp.customer_id,
					sp.reference_number,
					sp.payee_name,
					sp.remarks,
					sp.price,
					p.name AS plan_name,
					i.name AS isp_name,
					sp.subscription_id,
					DATE_FORMAT(sp.`date_created`, '%M %d, %Y') AS payment_date,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					pm.name as payment_method_name,
					pm.details as payment_method_details,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					c.email,
					sp.status
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 2 " . $where_query . "
				ORDER BY sp.date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_p_reject_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    i.name LIKE '%" . $search . "%'
				    OR
				    pm.name LIKE '%" . $search . "%'
				    OR
				    DATE_FORMAT(sp.`date_created`, '%M %d, %Y') LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 2 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function save_payment_subscription($data)
	{
		$sql = "UPDATE subscription_payment
				SET status 				= 1
				WHERE id 				= '".$data['id']."'";

        $this->db->query($sql);

		$sql2 = "UPDATE subscription
				SET next_payment 		= DATE_ADD(next_payment, INTERVAL 1 MONTH)
				WHERE id 				= '".$data['form_subscription_id']."'";

        $this->db->query($sql2);
        return $this->db->affected_rows();
	}

	function save_payment_rejection($data)
	{
		$sql = "UPDATE subscription_payment
				SET status 				= 2,
					remarks 			= '".$data['remarks']."'
				WHERE id 				= '".$data['id']."'";

        $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function add_payment_method($data)
	{
		$sql = "INSERT INTO payment_method(
					name, 
					details)
				VALUES(
				'".$data['name']."', 
				'".$data['details']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_payment_method($data)
	{
		$sql = "UPDATE payment_method
				SET name 			= '".$data['name']."',
					details 			= '".$data['details']."'
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_payment_method($data)
	{
		$sql = "UPDATE payment_method
				SET is_active = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_payment_method($limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
				    OR
				    details LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT 
					id,
					name,
					details,
					is_active,
					DATE_FORMAT(`date_created`, '%M %d, %Y') AS date_created
				FROM `payment_method`
				WHERE is_active = 0 " . $where_query . "
				ORDER BY date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_payment_method_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
				    OR
				    details LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `payment_method`
				WHERE is_active = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function save_notification($id = 0, $type = '', $details = '', $link = '')
	{
		$sql = "INSERT INTO notification(
					customer_id, 
					type,
					details,
					redirrect_link)
				VALUES(
				'".$id."', 
				'".$type."',
				'".$details."',
				'".$link."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function read_notification($data)
	{
		$sql = "UPDATE notification
				SET is_read 			= 1
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_number_of_notifications($id = 0){

		 $sql = "SELECT
					COUNT(*) AS number_of_notifications
				FROM `notification` 
				WHERE customer_id = " . $id . "
				AND is_read = 0";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_notification($id = 0, $limit = 0, $offset = 0, $search = ''){
		$where_query = '';
		$limit_query = '';

		if($search !=''){
			$where_query .= " AND (
				    type LIKE '%" . $search . "%'
				    OR
				    details LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}

		 $sql = "SELECT 
					*
				FROM notification
				WHERE customer_id = " . $id . "
				AND is_read = 0
				 " . $where_query . "
				ORDER BY date_created DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_notification_pagination($id = 0, $limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    type LIKE '%" . $search . "%'
				    OR
				    details LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM notification
				WHERE customer_id = " . $id . "
				AND is_read = 0  " . $where_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_year(){

		 $sql = "SELECT
					YEAR(`date_created`) AS year_name
				FROM `subscription_payment`
				WHERE STATUS = 1
				GROUP BY YEAR(`date_created`) 
				ORDER BY year_name DESC";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_validated_payment($year = 0, $month = 0)
	{
		$sql = "SELECT
					sp.id,
					sp.customer_id,
					sp.reference_number,
					sp.payee_name,
					sp.remarks,
					FORMAT(sp.price, 2) AS price,
					p.name AS plan,
					i.name AS isp,
					sp.subscription_id,
					DATE_FORMAT(sp.`date_created`, '%M %d, %Y') AS payment_date,
					CONCAT(c.address_line1, ' ', c.address_line2, ' ', ct.name, ' ', pr.name, ' ', c.zipcode) AS full_address,
					pm.name AS payment_method_name,
					pm.details AS payment_method_details,
					CONCAT(c.lname, ', ', c.fname) AS full_name,
					c.email,
					sp.status
				FROM `subscription_payment` sp
				INNER JOIN `subscription` s
				ON s.id = sp.subscription_id
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN `isp` i
				ON i.id = p.isp_id
				INNER JOIN customer c
				ON c.id = sp.customer_id
				INNER JOIN cities ct
				ON ct.id = c.city
				INNER JOIN provinces pr
				ON pr.id = c.province
				INNER JOIN payment_method pm
				ON pm.id = sp.payment_method
				WHERE sp.status = 1 
				AND YEAR(sp.`date_created`) = " . $year . "
				AND MONTH(sp.`date_created`) = " . $month . "
				ORDER BY sp.date_created ASC ";

        $result = $this->db->query($sql);
        return $result->result_array();
	}


	function get_analytics_isp($year){

		 $sql = "SELECT
					ip.month_name,
					GROUP_CONCAT(CONCAT(i.name, '  -  &#8369;', FORMAT(ip.`total_price`,2)) SEPARATOR ',<br>') total,
					FORMAT(SUM(ip.`total_price`),2) AS sub_total
				FROM (
					SELECT
						MONTH(sp.`date_created`) AS month_num,
						MONTHNAME(sp.`date_created`) AS month_name,
						p.`isp_id`,
						SUM(sp.`price`) AS total_price
					FROM `subscription_payment` sp
					INNER JOIN `subscription` s
					ON s.id = sp.`subscription_id`
					INNER JOIN plan p
					ON p.id = s.plan_id
					WHERE sp.status = 1
					AND YEAR(sp.date_created) = '" . $year . "'
					GROUP BY MONTHNAME(sp.`date_created`), p.`isp_id`
				) ip
				INNER JOIN isp i
				ON i.id = ip.isp_id
				GROUP BY MONTH_NAME
				ORDER BY month_num ASC";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_top_subscription(){

		 $sql = "SELECT
					i.name AS isp,
					p.name AS plan,
					COUNT(s.id) AS top_subscription
				FROM `subscription` s
				INNER JOIN `plan` p
				ON p.id = s.plan_id
				INNER JOIN isp i
				ON i.id = p.isp_id
				WHERE s.`status` = 0
				GROUP BY i.name, p.name
				ORDER BY top_subscription DESC
				LIMIT 10";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_total_active_users(){

		 $sql = "SELECT
					COUNT(id) AS total
				FROM `customer`
				WHERE is_active = 0";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_total_archive_users(){

		 $sql = "SELECT
					COUNT(id) AS total
				FROM `customer`
				WHERE is_active = 2";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_total_blocklist_users(){

		 $sql = "SELECT
					COUNT(id) AS total
				FROM `customer`
				WHERE is_active = 1";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_total_active_subscription(){

		 $sql = "SELECT
					COUNT(s.id) AS total
				FROM `subscription` s
				WHERE s.status= 0";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_total_disconnect_subscription(){

		 $sql = "SELECT
					COUNT(s.id) AS total
				FROM `subscription` s
				WHERE s.status= 1";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_analytics_total_terminate_subscription(){

		 $sql = "SELECT
					COUNT(s.id) AS total
				FROM `subscription` s
				WHERE s.status= 2";

        $result = $this->db->query($sql);
        return $result->result_array();
	}
}
?>