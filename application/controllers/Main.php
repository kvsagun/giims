<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Crud_model');
        $this->load->helper('array_helper');
    }
    
    public function is_logged_in() {
        $access = $this->session->userdata('giims_p_user_info');

        if(count($access) <= 0) {
            return false;
        } else {
            return $access;
        }
    }

	public function index()
	{
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Report';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/report.js?v1.01'];

            $data['_year'] = $this->Crud_model->get_year();

            $this->load->view('includes/header', $header);
            $this->load->view('report/index', $data);
            $this->load->view('includes/footer', $footer);
        }
	}

    // ISP

    public function isp()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'ISP';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/isp.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('isp/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_isp() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_isp();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_isp_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_isp($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_isp_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_isp_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_isp_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_isp($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_isp()
    {
        if($this->input->post()){
            if($this->input->post('action_type') == "create"){
                $result['data'] = $this->Crud_model->add_isp($this->input->post());
            }else if($this->input->post('action_type') == "update"){
                $result['data'] = $this->Crud_model->update_isp($this->input->post());
            }else{
                $result['data'] = $this->Crud_model->delete_isp($this->input->post());
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }


    // PLAN


    public function plan()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'plan';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/plan.js?v1.01'];

            $data['isp'] = $this->Crud_model->get_isp();

            $this->load->view('includes/header', $header);
            $this->load->view('plan/index', $data);
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_plan() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_plan();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_plan_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_plan($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_plan_by_isp_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_plan_by_isp_id($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_plan_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_plan_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_plan_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_plan($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_plan()
    {
        if($this->input->post()){
            if($this->input->post('action_type') == "create"){
                $result['data'] = $this->Crud_model->add_plan($this->input->post());
            }else if($this->input->post('action_type') == "update"){
                $result['data'] = $this->Crud_model->update_plan($this->input->post());
            }else{
                $result['data'] = $this->Crud_model->delete_plan($this->input->post());
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }


    // APPLICATON


    public function application()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
            }else{
            $header['nav']          = 'application';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/application.js?v1.05'];

            $this->load->view('includes/header', $header);
            $this->load->view('application/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_application() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_application();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_application_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_application($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_application_by_customer_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_application_by_customer_id($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_application_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_application_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_customer_application_pagination() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_customer_application_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'), $access['id']);

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_customer_application_table() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_application_by_customer_id($access['id'], $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_application_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_application($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_application()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->save_application($this->input->post());
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function approved_application()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->approved_application($this->input->post());
            $result['data2'] = $this->Crud_model->save_subscription($this->input->post());

            $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Application', 'Application Approved', base_url().'h/application');
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function reject_application()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->reject_application($this->input->post());
            
            $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Application', 'Rejected Application', base_url().'h/application');

            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }


    // Customer


    public function a_customer()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Active Customer';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/customer.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('customer/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function b_customer()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Blocklist Customer';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/b_customer.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('customer/blocklist');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_customer() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_customer();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function arc_customer()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Archive Customer';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/arc_customer.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('customer/archive');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_arc_customer() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_arc_customer();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_arc_customer_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_arc_customer_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_arc_customer_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_arc_customer($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_status_of_customer()
    {
        if($this->input->post()){
            if($this->input->post('action_type') == "blocklist"){
                $result['data'] = $this->Crud_model->save_status_of_customer($this->input->post(), 1);
            }else if($this->input->post('action_type') == "archive"){
                $result['data'] = $this->Crud_model->save_status_of_customer($this->input->post(), 2);
            }else{
                $result['data'] = $this->Crud_model->save_status_of_customer($this->input->post(), 0);
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function get_b_customer() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_b_customer();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_b_customer_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_b_customer_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_b_customer_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_b_customer($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_customer_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_customer($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_customer_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_customer_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_customer_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_customer($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }


    // personnel

    public function personnel()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Personnel';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/personnel.js?v1.0'];

            $this->load->view('includes/header', $header);
            $this->load->view('personnel/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_personnel() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_personnel();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_personnel_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_personnel($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_personnel_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_personnel_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_personnel_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_personnel($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_personnel()
    {
        if($this->input->post()){
            if($this->input->post('action_type') == "create"){
                $result['data'] = $this->Crud_model->add_personnel($this->input->post());
            }else if($this->input->post('action_type') == "update"){
                $result['data'] = $this->Crud_model->update_personnel($this->input->post());
            }else{
                $result['data'] = $this->Crud_model->delete_personnel($this->input->post());
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function get_provinces() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_provinces();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_city_by_province_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_city_by_province_id($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    // subscription

    public function a_subscription()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Subscription';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/subscription.js?v1.04'];

            $this->load->view('includes/header', $header);
            $this->load->view('subscription/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_a_subscription() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_a_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_a_subscription_by_customer_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_a_subscription_by_customer_id($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_a_subscription_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_a_subscription_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_a_subscription_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_a_subscription($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function d_subscription()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Disconnected Subscription';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/d_subscription.js?v1.04'];

            $this->load->view('includes/header', $header);
            $this->load->view('subscription/d_subscription');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_d_subscription() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_d_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_d_subscription_by_customer_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_d_subscription_by_customer_id($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_d_subscription_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_d_subscription_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_d_subscription_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_d_subscription($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function t_subscription()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Terminated Subscription';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/t_subscription.js?v1.03'];

            $this->load->view('includes/header', $header);
            $this->load->view('subscription/t_subscription');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_t_subscription() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_t_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_t_subscription_by_customer_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_t_subscription_by_customer_id($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_t_subscription_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_t_subscription_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_t_subscription_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_t_subscription($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_status_of_subscription()
    {
        if($this->input->post()){
            if($this->input->post('action_type') == "disconnect"){
                $result['data'] = $this->Crud_model->disconnect_subscription($this->input->post());
                $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Subscription', 'Subscription Disconnected', base_url().'h/account');
            }else if($this->input->post('action_type') == "terminate"){
                $result['data'] = $this->Crud_model->terminite_subscription($this->input->post());
                $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Subscription', 'Subscription Terminated', base_url().'h/account');
            }else{
                $result['data'] = $this->Crud_model->reconnect_subscription($this->input->post());
                $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Subscription', 'Subscription Reconnected', base_url().'h/account');
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function get_customer_all_subscription() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_customer_all_subscription($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_customer_due_subscription() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_customer_due_subscription($id = $this->input->post('id'), $s_id = $this->input->post('subscription_id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_subscription_payment()
    {
        if($this->input->post()){
            if($this->input->post('subscription_id') == "all"){
                $result['data'] = $this->Crud_model->subscription_payment_all($this->input->post());
            }else {
                $result['data'] = $this->Crud_model->subscription_payment($this->input->post());
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function get_subscription_pagination_by_customer_id() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');
            $result['data'] = $this->Crud_model->get_subscription_pagination_by_customer_id($access['id'], $limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_subscription_by_customer_id_table() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_subscription_by_customer_id($access['id'], $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_customer_subscription_payment() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_subscription_payment_by_customer_id($access['id'], $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_subscription_payment_by_customer_id_pagination() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_subscription_payment_by_customer_id_pagination($access['id'], $limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function payment_validation()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Payment Validation';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/payment_validation.js?v1.03'];

            $this->load->view('includes/header', $header);
            $this->load->view('payment/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_payment_validation()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_payment_validation();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_payment_validation_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_payment_validation_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_get_payment_validation_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_payment_validation($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function p_validated()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Payment Validated';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/p_validated.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('payment/p_validated');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_validated_payment()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_validated_payment();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_validated_payment_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_validated_payment_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_validated_payment_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_validated_payment($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function p_reject()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Payment Reject';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/p_reject.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('payment/p_reject');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_p_reject()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_p_reject();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_p_reject_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_p_reject_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_p_reject_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_p_reject($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_payment_subscription()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->save_payment_subscription($this->input->post());

            $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Payment', 'Payment Validated', base_url().'h/account');
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function save_payment_rejection()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->save_payment_rejection($this->input->post());

            $result['notification'] = $this->Crud_model->save_notification($this->input->post('customer_id'), 'Payment', 'Rejected Payment', base_url().'h/account');
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function payment_method()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Payment Method';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/payment_method.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('payment_method/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_payment_method() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_payment_method();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_payment_method_pagination() 
    {
        if($this->input->post()){

            $result['data'] = $this->Crud_model->get_payment_method_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_payment_method_table() 
    {
        if($this->input->post()){

            $result['data'] = $this->Crud_model->get_payment_method($limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_payment_method()
    {
        if($this->input->post()){
            if($this->input->post('action_type') == "create"){
                $result['data'] = $this->Crud_model->add_payment_method($this->input->post());
            }else if($this->input->post('action_type') == "update"){
                $result['data'] = $this->Crud_model->update_payment_method($this->input->post());
            }else{
                $result['data'] = $this->Crud_model->delete_payment_method($this->input->post());
            }
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function dashboard()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Dashboard';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/dashboard.js?v1.01'];

            $this->load->view('includes/header', $header);
            $this->load->view('dashboard/index');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_notification() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_notifications($access['id']);

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_notification_pagination() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_notification_pagination($access['id'], $limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_notification_table() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_notification($access['id'], $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_number_of_notifications() 
    {
        if($this->input->post()){
            $access = $this->session->userdata('giims_user_info');

            $result['data'] = $this->Crud_model->get_number_of_notifications($access['id']);

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function read_notification()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->read_notification($this->input->post());
            
            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

    public function report()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."c/p_login", true, 301);     
        }else{
            $header['nav']          = 'Report';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/report.js?v1.03'];

            $data['_year'] = $this->Crud_model->get_year();

            $this->load->view('includes/header', $header);
            $this->load->view('report/index', $data);
            $this->load->view('includes/footer', $footer);
        }
    }

    public function get_report()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_report();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_isp()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_isp($this->input->post('year'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_validated_payment()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_validated_payment($this->input->post('year'), $this->input->post('month'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_top_subscription()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_top_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_total_active_users()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_total_active_users();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_total_archive_users()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_total_archive_users();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_total_blocklist_users()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_total_blocklist_users();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_total_active_subscription()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_total_active_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_total_disconnect_subscription()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_total_disconnect_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_analytics_total_terminate_subscription()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_analytics_total_terminate_subscription();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_report_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_report_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_report_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_report($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

}
