<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C extends CI_Controller {

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Crud_model');
        $this->load->helper('array_helper');
    }
    
	public function is_logged_in() {
		// $access = $this->session->userdata('giims_user_info');
		
		// if(array_check($access)) {
		// 	header("Location: ".base_url()."h", true, 301);		
		// }

		// $access2 = $this->session->userdata('giims_p_user_info');
		
		// if(array_check($access2)) {
		// 	header("Location: ".base_url()."main", true, 301);		
		// }
        $access = $this->session->userdata('giims_user_info');

        if(count($access) <= 0) {
            return false;
        } else {
            return $access;
        }
	}

	public function index()
	{
        $user_session = $this->is_logged_in();
        if($user_session == false){
			$data['nav'] 			= 'Login';
			$data['css']			= ['signin.css?v=1.01'];
			$data['javascripts']	= ['modules/login.js?v1.01'];

	        $this->load->view('login/c_login',$data);
        }else{
            header("Location: ".base_url()."h", true, 301);     
	    }
	}

	public function register()
	{
        $user_session = $this->is_logged_in();
        if($user_session == false){
			$data['nav'] 			= 'Register';
			$data['css']			= ['signin.css?v=1.01'];
			$data['javascripts']	= ['modules/login.js?v1.01'];

            $data['province'] = $this->Crud_model->get_provinces();
	        $this->load->view('login/register',$data);
        }else{
            header("Location: ".base_url()."h", true, 301);     
	    }
	}

	public function authorize_user()
	{
        if(!$this->input->post()){
        	show_404();
        }

		$login_details = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		);
		
		$result = $this->Crud_model->authorize_user($login_details);

		if(count($result) > 0) {

			if($result['is_active'] == 0) {

				unset($result['password']);
				$this->session->set_userdata('giims_user_info', $result);
				$response['message'] = base_url() . "h";				
				$response['status'] = true;
				echo json_encode($response);

			} else {

				$response['status'] = false;
				$response['message'] = "Please contact the administrator for your account";
				echo json_encode($response);

			}
			
		} else {

			$response['status'] = false;
			$response['message'] = "Incorrect Username or Password";
			echo json_encode($response);

		}
	}


    public function register_customer()
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->register_customer($this->input->post());
            $result['message'] = base_url() . "h";

            $access = $this->Crud_model->get_customer($result['data']);

            $this->session->set_userdata('giims_user_info', $access[0]);

            header("Content-Type: application/json", true);
            $this->output->set_output(print(json_encode($result)));
            exit();
        }else{
            show_404();
        }
    }

	public function logout()
	{
		$this->session->unset_userdata('giims_user_info');
		header("Location: ".base_url()."c", true, 301);
	}

	public function p_login()
	{
		$access = $this->session->userdata('giims_p_user_info');

        if(count($access) <= 0) {
			$data['nav'] 			= 'Personnel Login';
			$data['css']			= ['signin.css?v1.01'];
			$data['javascripts']	= ['modules/login.js?v1.01'];

	        $this->load->view('login/p_login',$data);
        }else{
            header("Location: ".base_url()."Main", true, 301);     
        }
	}

	public function p_logout()
	{
		$this->session->unset_userdata('giims_p_user_info');
		header("Location: ".base_url()."c/p_login", true, 301);
	}

	public function authorize_personnel()
	{
        if(!$this->input->post()){
        	show_404();
        }

		$login_details = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		);
		
		$result = $this->Crud_model->authorize_personnel($login_details);

		if(count($result) > 0) {

			if($result['is_active'] == 0) {

				unset($result['password']);
				$this->session->set_userdata('giims_p_user_info', $result);
				switch ($result['role']) {
					case 'admin':
						$response['message'] = base_url() . "main";		
						break;
					case 'employee':
						$response['message'] = base_url() . "main/application";		
						break;
					case 'collector':
						$response['message'] = base_url() . "main/payment_validation";		
						break;
					
					default:
						# code...
						break;
				}
				$response['status'] = true;
				echo json_encode($response);

			} else {

				$response['status'] = false;
				$response['message'] = "Please contact the administrator for your account";
				echo json_encode($response);

			}
			
		} else {

			$response['status'] = false;
			$response['message'] = "Incorrect Username or Password";
			echo json_encode($response);

		}
	}


}
