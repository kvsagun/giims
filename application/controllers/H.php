<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class H extends CI_Controller {

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Crud_model');
        $this->load->helper('array_helper');
    }
    
    public function is_logged_in() {
        $access = $this->session->userdata('giims_user_info');

        if(count($access) <= 0) {
            return false;
        } else {
            return $access;
        }
    }

    public function index()
    {
        $header['nav']          = 'Home';
        $header['css']          = [''];
        $footer['javascripts']  = ['modules/home.js?v1.02'];

        $data['isp']            = $this->Crud_model->get_isp();
        $data['user_info']      = $this->session->userdata('giims_user_info');
        $data['province']       = $this->Crud_model->get_provinces();

        $header['num_notifications'] = $this->Crud_model->get_number_of_notifications(($data['user_info']['id']) ? $data['user_info']['id'] : 0);
        // $header['num_notifications'] = $this->Crud_model->get_number_of_notifications($data['user_info']['id']);
        
        $this->load->view('includes/s_header', $header);
        $this->load->view('Home/index', $data);
        $this->load->view('includes/footer', $footer);
    }

    public function application()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."h", true, 301);     
        }else{
            $header['nav']          = 'Application';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/c_application.js?v1.03'];

            $data['user_info']      = $this->session->userdata('giims_user_info');

            $header['num_notifications'] = $this->Crud_model->get_number_of_notifications(($data['user_info']['id']) ? $data['user_info']['id'] : 0);

            $this->load->view('includes/s_header', $header);
            $this->load->view('home/application');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function account()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."h", true, 301);     
        }else{
            $header['nav']          = 'Account';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/account.js?v1.03'];

            $data['user_info']      = $this->session->userdata('giims_user_info');

            $header['num_notifications'] = $this->Crud_model->get_number_of_notifications(($data['user_info']['id']) ? $data['user_info']['id'] : 0);

            $this->load->view('includes/s_header', $header);
            $this->load->view('home/account');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function notification()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."h", true, 301);     
        }else{
            $header['nav']          = 'Notification';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/notification.js?v1.01'];

            $data['user_info']      = $this->session->userdata('giims_user_info');

            $header['num_notifications'] = $this->Crud_model->get_number_of_notifications(($data['user_info']['id']) ? $data['user_info']['id'] : 0);

            $this->load->view('includes/s_header', $header);
            $this->load->view('home/notification');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function subscription()
    {
        $user_session = $this->is_logged_in();
        if($user_session == false){
            header("Location: ".base_url()."h", true, 301);     
        }else{
            $header['nav']          = 'Subscription';
            $header['css']          = [''];
            $footer['javascripts']  = ['modules/c_subscription.js?v1.0'];

            $data['user_info']      = $this->session->userdata('giims_user_info');

            $header['num_notifications'] = $this->Crud_model->get_number_of_notifications(($data['user_info']['id']) ? $data['user_info']['id'] : 0);

            $this->load->view('includes/s_header', $header);
            $this->load->view('home/subscription');
            $this->load->view('includes/footer', $footer);
        }
    }

    public function terms($id = 0)
    {
        $data['terms'] = $this->Crud_model->get_plan($id);

        $this->load->view('home/terms', $data);
    }
}
