<main role="main" class="col-md-12 pt-3 px-4">
  <div class="container-fluid">

  	<div class="step-1">
	  <div class="col-lg-12">
	  	<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
	      <h1 class="display-4">Step 1: Select Internet Connection Type</h1>
	    </div>

	    <div class="container">
	      <div class="card-deck mb-3 text-center">
        	<?php 
        	foreach($isp as $row){
        		echo '<div class="card mb-4 box-shadow">
				          <div class="card-header badge-danger">
				            <h4 class="my-0 font-weight-normal">'.$row['name'].'</h4>
				          </div>
				          <div class="card-body">
				            <button type="button" class="btn btn-lg btn-block btn-outline-dark" onclick="step2(\''.$row['id'].'\', \''.$row['name'].'\')">Select</button>
				          </div>
				        </div>';
            }?>	
	        </div>
	      </div>
	    </div>
	</div>

	<div class="step-2" style="display: none;">
		<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
	      	<h1 class="display-4">Step 2: Select Intenet Plan</h1>
      		<span class="text-muted">Select other internet connection type? <span><a href="#" onclick="step1()">Back to Step 1</a>
	    </div>

	    <div class="container">
	      <div class="card-deck mb-3 text-center table-list">
	      </div>
	    </div>
	</div>

    <div class="card mb-4 box-shadow table-list-template" style="display:none;">
      <div class="card-header badge-danger">
        <h4 class="my-0 font-weight-normal plan-name"></h4>
      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title"><label class="price"></label> <small class="text-muted">/ month</small></h1>
        <ul class="list-unstyled mt-3 mb-4 description">
        </ul>
        <button type="button" class="btn btn-lg btn-block btn-outline-dark selected-plan">Apply now</button>
      </div>
    </div>
    <!--
		FORM FOR CREATE AND UPDATE
    -->

	  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-lg" role="document">
	      <div class="modal-content">
	        <div class="modal-header badge-dark">
	          <h5 class="modal-title" id="exampleModalLabel">Application</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <form id="_form">
	          <div class="modal-body">
	            <div class="col-12">
	                <input type="hidden" name="id" id="id">
                	<input type="hidden" value="create" name="action_type" id="action_type">
                	<input type="hidden" value="" name="isp" id="isp">
                	<input type="hidden" value="" name="plan" id="plan">
                	<input type="hidden" value="" name="total_price" id="total_price">
                	<input type="hidden" value="<?= $user_info['id']?>" name="customer_id" id="customer_id">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Customer</label>
						<div class="col-sm-10">
							<label class="col-form-label form-customer"><?= $user_info['lname']. ', '. $user_info['fname']?></label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">ISP</label>
						<div class="col-sm-10">
							<label class="col-form-label form-isp"></label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Plan</label>
						<div class="col-sm-10">
							<label class="col-form-label form-plan"></label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Price</label>
						<div class="col-sm-10">
							<label class="col-form-label form-price"></label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Location</label>
						<div class="col-sm-5">
			                <input type="text" name="line1" id="line1" required="true" class="form-control" placeholder="Address Line 1">
						<small class="col-form-label text-muted">Street address</small>
						</div>
						<div class="col-sm-5">
			                <input type="text" name="line2" id="line2" required="true" class="form-control" placeholder="Address Line 2">
						<small class="col-form-label text-muted">Appartment, unit, building, floor, etc.</small>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"></label>
						<div class="col-sm-10">
					      <select name="province" id="province" class="form-control" required="" autofocus>
						      <option disabled="" selected="">Select Province</option>
						        <?php 
						          foreach($province as $row){
						            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
						        }?>                         
					      </select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"></label>
						<div class="col-sm-10">
					      <select name="city" id="city" class="form-control" required="" autofocus>
					      	<option disabled="" selected="">Select City</option>       
					      </select>           
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"></label>
						<div class="col-sm-10">
			                <input type="text" name="zipcode" id="zipcode" required="true" class="form-control" placeholder="Zipcode">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"></label>
						<div class="col-sm-10">
			                <input type="text" name="contact_number" id="contact_number" required="true" class="form-control" placeholder="Contact Number">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"></label>
						<div class="col-sm-10">
			                <input type="checkbox" class="custom-control-input" id="terms">
  							<label class="custom-control-label" for="terms">&nbsp;I agree to the <a href="#" target="_blank" class="form-terms">terms and contract</a> of the internet provider</label>
						</div>
					</div>
	            </div>
	          </div>
	          <div class="modal-footer">
	            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
	            <button type="submit" class="btn btn-sm btn-primary approve" disabled>Submit</button>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
	    
</main>