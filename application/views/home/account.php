<main role="main" class="col-md-12 pt-3 px-4">
  <div class="container-fluid">

	  <div class="card">

	    <div class="card-body">
	    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			    <h1 class="h2">My Account</h1>
		  	</div>

			  <div class="col-lg-12">
			    <div class="row justify-content-between">
			      <div class="col-md-9">        
			        <div class="form-group row">
					    <h3>Your balance</h3>
				  	</div>
			      </div>
			      <div class="col-md-3">        
			        <div class="form-group">
					    <h2 class="balance">&#8369;0.00</h2>
			        </div>
			      </div>
			    </div>
			  </div>

			  <div class="col-lg-12">
			    <div class="row justify-content-between">
			      <div class="col-md-9">      
			        <div class="form-group">
			      		<label>Internet Plan: <span class="plan-name">
			        	</span></label>	
			        </div>
			      </div>
			      <div class="col-md-3">        
			        <div class="form-group">
				    	<a href="#" class="btn-add make-payment">Make Payment</a>
			        </div>
			      </div>
			    </div>
			   </div>

	    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			    <h1 class="h2">Your Payments</h1>
		  	</div>

			  <div class="col-lg-12">
			    <div class="row justify-content-between">
			      <div class="col-md-3">        
			        <div class="form-group row">
					    <label for="staticEmail" class="col-sm-5 col-form-label">Show</label>
					    <div class="col-sm-7">
					      <select class="form-control show-entries">
					      	<option>10</option>
					      	<option>50</option>
					      	<option>100</option>
					      </select>
					    </div>
					  </div>
			      </div>
			      <div class="col-md-3">        
			        <div class="form-group">
			            <input type="text" name="search" class="form-control search " placeholder="Search">
			        </div>
			      </div>
			    </div>

			    <div class="table-responsive">
				    <table class="table table-hover">
				      <thead>
				        <th>#</th>
				        <th>Payment Date</th>
				        <th>Subscription</th>
				        <th>Location</th>
				        <th>Price</th>
				        <th>Status</th>
				        <th>Action</th>
				      </thead>

				        <tr class="table-list-template" style="display:none;">
				          <th class="count"></th>
				          <td class="payment-date"></td>
				          <td class="subscription"></td>
				          <td class="location"></td>
				          <td class="price"></td>
				          <td class="status"></td>
				          <td>
				          	<a href="#" class="view">View Invoice</a>
				          </td>
				        </tr>   


					    <!--
							POPULATE DATA
					    -->

				      <tbody class="table-list">
				             
				      </tbody>

				    </table>
			    </div>

			    <!--
					POPULATE PAGINATION
			    -->

			    <li class="page-item pagination-template" style="display: none"><a class="page-link" href="#" offset="">1</a></li>

			    <div class="load-loader text-center" style="display:none;"><img src="<?php echo base_url() ?>assets/img/loader.gif"></div>
			    <div class="row justify-content-between">
			        <button class="btn btn-outline-info btn-previous"><span data-feather="chevron-left"></span></button>
			        <ul class="pagination">
			        </ul>
			        <button class="btn btn-outline-primary btn-next"><span data-feather="chevron-right"></span></button>
			    </div>
			  </div>

			  <br>
			  <br>


		    <!--
				FORM FOR CREATE AND UPDATE
		    -->

			  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			      <div class="modal-content">
			        <div class="modal-header badge-dark">
			          <h5 class="modal-title" id="exampleModalLabel">Make Payment</h5>
			          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			            <span aria-hidden="true">&times;</span>
			          </button>
			        </div>
			        <form id="_form">
			          <div class="modal-body">
			            <div class="col-12">
			                <input type="hidden" name="id" id="id">
			                <input type="hidden" name="price" id="price">
		                	<input type="hidden" value="create" name="action_type" id="action_type">
		                	<input type="hidden" value="<?= $this->session->userdata('giims_user_info')['id']?>" name="customer_id" id="customer_id">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label"></label>
								<div class="col-sm-6">
									<h2 class="total-price text-center">&#8369;0.00</h2>
								</div>
								<label class="col-sm-3 col-form-label"></label>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Select Payment Method</label>
								<div class="col-sm-10">	
				                  <select name="payment_method" id="payment_method" class="form-control">
				                  </select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Account Details</label>
								<div class="col-sm-10">	
									<label class="col-form-label form-payment-details"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Select Plan</label>
								<div class="col-sm-10">	
				                  <select name="subscription_id" id="subscription_id" class="form-control">
						            <option value="all" selected="">All</option>	                  	
				                  </select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Customer</label>
								<div class="col-sm-10">
									<label class="col-form-label form-customer"><?= $this->session->userdata('giims_user_info')['lname'] . ', ' . $this->session->userdata('giims_user_info')['fname'] ?></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Subscription</label>
								<div class="col-sm-10">
									<label class="col-form-label form-subs"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Reference Number</label>
								<div class="col-sm-10">
			                  		<input type="text" name="reference_number" id="reference_number" required="true" class="form-control" placeholder="Reference Number/ Transaction ID">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Payee Name</label>
								<div class="col-sm-10">
			                  		<input type="text" name="payee_name" id="payee_name" required="true" class="form-control" placeholder="Payee Name">
								</div>
							</div>
			            </div>
			          </div>
			          <div class="modal-footer">
			            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
			            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
			          </div>
			        </form>
			      </div>
			    </div>
			  </div>



			  <div class="modal fade" id="viewDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			      <div class="modal-content">
			        <div class="modal-header badge-dark">
			          <h5 class="modal-title" id="exampleModalLabel">Invoice</h5>
			          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			            <span aria-hidden="true">&times;</span>
			          </button>
			        </div>
			        <form id="_form">
			          <div class="modal-body">
			            <div class="col-12 print-area" id="printArea">
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Payment Method</label>
								<div class="col-sm-9">
									<label class="col-form-label form-payment-method"></label>
								</div>
								<label class="col-sm-1"><a href="#" class="print"><span data-feather="printer"></span></a></label>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Customer</label>
								<div class="col-sm-10">
									<label class="col-form-label form-customer"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Email</label>
								<div class="col-sm-10">
									<label class="col-form-label form-email"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Full Address</label>
								<div class="col-sm-10">
									<label class="col-form-label form-location"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Subscription</label>
								<div class="col-sm-10">
									<label class="col-form-label form-subscription"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Price</label>
								<div class="col-sm-10">
									<label class="col-form-label form-price"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Reference Number</label>
								<div class="col-sm-10">
									<label class="col-form-label form-reference-number"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Payee Name</label>
								<div class="col-sm-10">
									<label class="col-form-label form-payee-name"></label>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Remaks</label>
								<div class="col-sm-10">
									<b class="col-form-label form-remarks text-danger"></b>
								</div>
							</div>
			            </div>
			          </div>
			          <div class="modal-footer">
			            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
			          </div>
			        </form>
			      </div>
			    </div>
			  </div>
	    </div>
	    
	  </div>

	</div>
</main>