<main role="main" class="col-md-12 pt-3 px-4">
  <div class="container-fluid">

	  <div class="card">

	    <div class="card-body" id="printAreaAll">
	    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			    <h1 class="h2">Report</h1>
			    <div class="btn-toolbar mb-2 mb-md-0">
				    <!-- <a href="#" class="btn-add">Add Internet Plan</a> -->
			    </div>
			</div>

	  		<br>
	  		<div class="row justify-content-end">
	  			<div class="col-md-1">
	  				<a href="#" class="print-all"><span data-feather="printer"></span> Print</a>
	  			</div>
	  		</div>
			<div class="col-md-12">
              <br>
				<div class="row">
					<div class="col-md-4">
						<div class="card">
						  <div class="card-header badge-success">
						    Active Customer
						  </div>
						  <div class="card-body">
						    <h5 class="card-title active-user">420</h5>
						  </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
						  <div class="card-header badge-warning">
						    Archived Customer
						  </div>
						  <div class="card-body">
						    <h5 class="card-title archive-user">420</h5>
						  </div>
						</div>
					</div>		
					<div class="col-md-4">
						<div class="card">
						  <div class="card-header badge-danger">
						    Blocklisted Customer
						  </div>
						  <div class="card-body">
						    <h5 class="card-title blocklist-user">420</h5>
						  </div>
						</div>
					</div>		
				</div>
				<br>
				<div class="row">
					<div class="col-md-4">
						<div class="card">
						  <div class="card-header badge-success">
						    Active Subscription
						  </div>
						  <div class="card-body">
						    <h5 class="card-title active-subscription">420</h5>
						  </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
						  <div class="card-header badge-warning">
						    Disconnected Subscription
						  </div>
						  <div class="card-body">
						    <h5 class="card-title disconnect-subscription">420</h5>
						  </div>
						</div>
					</div>		
					<div class="col-md-4">
						<div class="card">
						  <div class="card-header badge-danger">
						    Terminated Discription
						  </div>
						  <div class="card-body">
						    <h5 class="card-title terminate-subscription">420</h5>
						  </div>
						</div>
					</div>		
				</div>
			</div>

			<br>

			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">						
						<div class="card">
						  <div class="card-header">
						    Internet Connection Type Report
						  </div>
						  <div class="card-body">
				              <select name="year" id="year" class="form-control">
				            	<?php 
				            	foreach($_year as $row){
					                echo '<option>'.$row['year_name'].'</option>';
					            }?>			                  	
				              </select>
					  		<br>
					  		<div class="row justify-content-end">
					  			<div class="col-md-2">
					  				<a href="#" class="print2"><span data-feather="printer"></span> Print</a>
					  			</div>
					  		</div>
					  		<br>
						    <table class="table" id="printArea2">
						    	<thead>
						    		<tr>
						    			<th>Month</th>
						    			<th>Internet Type</th>
						    			<th>Total</th>
						    		</tr>
						    	</thead>
						    	<tbody class="isp-table-total">
						    		<tr>
						    			<td>January</td>
						    			<td>PLDT - 420</td>
						    			<td>420</td>
						    		</tr>						    		
						    	</tbody>
						    </table>
						  </div>
						</div>
					</div>

					<div class="col-md-6">						
						<div class="card">
						  <div class="card-header">
						    Top Subscription
						  </div>
						  <div class="card-body">
					  		<div class="row justify-content-end">
					  			<div class="col-md-2">
					  				<a href="#" class="print3"><span data-feather="printer"></span> Print</a>
					  			</div>
					  		</div>
					  		<br>
						    <table class="table" id="printArea3">
						    	<thead>
						    		<tr>
						    			<th>Subscription</th>
						    			<th>Total Count</th>
						    		</tr>
						    	</thead>
						    	<tbody class="subscription-table-count">
						    		<tr>
						    			<td>PLDT - unli fbr 1499</td>
						    			<td>420</td>
						    		</tr>						    		
						    	</tbody>
						    </table>
						  </div>
						</div>
					</div>
				</div>				
			</div>

			<br>

			<div class="col-md-12">						
				<div class="card">
				  <div class="card-header">
				    Validated Payment
				  </div>
				  <div class="card-body">
			  		<div class="row">
			  			<div class="col-md-6">
			              <select name="year2" id="year2" class="form-control">
			            	<?php 
			            	foreach($_year as $row){
				                echo '<option>'.$row['year_name'].'</option>';
				            }?>			                  	
			              </select>
			  			</div>
			  			<div class="col-md-6">
			              <select name="month" id="month" class="form-control">
			              	<option value="1">January</option>		                  	
			              	<option value="2">Febuary</option>		                  	
			              	<option value="3">March</option>		                  	
			              	<option value="4">April</option>		                  	
			              	<option value="5">May</option>		                  	
			              	<option value="6">June</option>		                  	
			              	<option value="7">July</option>		                  	
			              	<option value="8">August</option>		                  	
			              	<option value="9">September</option>		                  	
			              	<option value="10">October</option>		                  	
			              	<option value="11">November</option>		                  	
			              	<option value="12">December</option>		                  	
			              </select>
			  			</div>
			  		</div>
			  		<br>
			  		<div class="row justify-content-end">
			  			<div class="col-md-1">
			  				<a href="#" class="print"><span data-feather="printer"></span> Print</a>
			  			</div>
			  		</div>
			  		<br>
				    <table class="table" id="printArea">
				    	<thead>
				    		<tr>
				    			<th>Date</th>
				    			<th>Name</th>
				    			<th>Subscription</th>
				    			<th>Total</th>
				    		</tr>
				    	</thead>
				    	<tbody class="validated-table">
				    		<tr>
				    			<td>January</td>
				    			<td>PLDT - 420</td>
				    			<td>PLDT - 420</td>
				    			<td>420</td>
				    		</tr>						    		
				    	</tbody>
				    </table>
				  </div>
				</div>
			</div>


	    </div>
	    
	  </div>

	</div>
</main>
