<main role="main" class="col-md-12 pt-3 px-4">
  <div class="container-fluid">

	  <div class="card">

	    <div class="card-body">
	    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			    <h1 class="h2">Dashboard</h1>
			    <div class="btn-toolbar mb-2 mb-md-0">
				    <!-- <a href="#" class="btn-add">Add Internet Plan</a> -->
			    </div>
			</div>

          	<div id="myChart"></div>

	    </div>
	    
	  </div>

	</div>
</main>
<script src="https://cdn.zingchart.com/zingchart.min.js"></script>