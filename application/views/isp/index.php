<main role="main" class="col-md-12 pt-3 px-4">
  <div class="container-fluid">

	  <div class="card">

	    <div class="card-body">
	    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			    <h1 class="h2">Internet Connection Type</h1>
			    <div class="btn-toolbar mb-2 mb-md-0">
				    <a href="#" class="btn-add">Add Internet Connection Type</a>
			        <!-- <button class="btn btn-dark btn-add">Add ISP</button> -->
			    </div>
			  </div>

			  <div class="col-lg-12">
			    <div class="row justify-content-between">
			      <div class="col-md-3">        
			        <div class="form-group row">
					    <label for="staticEmail" class="col-sm-5 col-form-label">Show</label>
					    <div class="col-sm-7">
					      <select class="form-control show-entries">
					      	<option>10</option>
					      	<option>50</option>
					      	<option>100</option>
					      </select>
					    </div>
					  </div>
			      </div>
			      <div class="col-md-3">        
			        <div class="form-group">
			            <input type="text" name="search" class="form-control search " placeholder="Search">
			        </div>
			      </div>
			    </div>

			    <div class="table-responsive">
				    <table class="table table-hover">
				      <thead>
				        <th>#</th>
				        <th>Name</th>
				        <th>Action</th>
				      </thead>

				        <tr class="table-list-template" style="display:none;">
				          <th class="count"></th>
				          <td class="name"></td>
				          <td>
				          	<a href="#" class="edit">edit</a>
				          	<a href="#" class="delete">delete</a>
			            	<!-- <button class="btn btn-sm btn-primary edit"><span class="feather-16" data-feather="edit-2"></span></button>
				            <button class="btn btn-sm btn-danger delete"><span class="feather-16" data-feather="trash-2"></span></button> -->
				          </td>
				        </tr>   


					    <!--
							POPULATE DATA
					    -->

				      <tbody class="table-list">
				             
				      </tbody>

				    </table>
			    </div>

			    <!--
					POPULATE PAGINATION
			    -->

			    <li class="page-item pagination-template" style="display: none"><a class="page-link" href="#" offset="">1</a></li>

			    <div class="load-loader text-center" style="display:none;"><img src="<?php echo base_url() ?>assets/img/loader.gif"></div>
			    <div class="row justify-content-between">
			        <button class="btn btn-outline-info btn-previous"><span data-feather="chevron-left"></span></button>
			        <ul class="pagination">
			        </ul>
			        <button class="btn btn-outline-primary btn-next"><span data-feather="chevron-right"></span></button>
			    </div>
			  </div>

			  <br>
			  <br>


		    <!--
				FORM FOR CREATE AND UPDATE
		    -->

			  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			      <div class="modal-content">
			        <div class="modal-header badge-dark">
			          <h5 class="modal-title" id="exampleModalLabel">Internet Connection Type</h5>
			          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			            <span aria-hidden="true">&times;</span>
			          </button>
			        </div>
			        <form id="_form">
			          <div class="modal-body">
			            <div class="col-12">
			                <input type="hidden" name="id" id="id">
		                	<input type="hidden" value="create" name="action_type" id="action_type">
			                <div class="form-group">           
			                </div>
			                <div class="form-group">              
			                  <input type="text" name="name" id="name" required="true" class="form-control" placeholder="Internet Connection Type Name">
			                </div>
			            </div>
			          </div>
			          <div class="modal-footer">
			            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
			            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
			          </div>
			        </form>
			      </div>
			    </div>
			  </div>
	    </div>
	    
	  </div>

	</div>
</main>