<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="GIIMS">
    <meta name="author" content="kvsagun@gmail.com">
    <link rel="icon" href="<?= base_url();?>assets/img/Logo.png">

    <title><?= $nav?> - GIIMS</title>
    
    <script src="<?= base_url();?>assets/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
    
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">

    <link href="<?= base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/sweetalert2.min.js"></script>

    <link href="<?= base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>
    <!-- Custom styles for this template -->
    <!-- <link href="signin.css" rel="stylesheet"> -->

    <?php if(isset($css) AND array_check($css)): ?>
      <?php foreach ($css as $css_value) : ?>
        <link href="<?= base_url() . 'assets/css/' . $css_value?>" rel="stylesheet" type="text/css" />
      <?php endforeach;?>
    <?php endif;?>

    <script type="text/javascript">
      var base_url  = "<?= base_url();?>";
    </script>

  </head>

  <body class="text-center">
    <form class="form-signin" id="_form_p_login">
      <img src="<?= base_url();?>assets/img/Logo.png" alt="" width="200" height="200">
      <h1 class="h3 mb-3 font-weight-normal">Personnel Sign in</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-dark btn-block" type="submit">Sign in</button>
      <a href="<?= base_url()?>c">Login as Customer</a>
      <span id="message"><span>
    </form>
  </body>

    <?php if(isset($javascripts) AND array_check($javascripts)): ?>
      <?php foreach ($javascripts as $javascript) : ?>
        <?php if($javascript != ''):?>
          <script src="<?= base_url() . 'assets/js/' . $javascript?>"></script>
        <?php endif;?>
      <?php endforeach;?>
    <?php endif;?>

</html>
