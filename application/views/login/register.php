<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="GIIMS">
    <meta name="author" content="kvsagun@gmail.com">
    <link rel="icon" href="<?= base_url();?>assets/img/Logo.png">

    <title><?= $nav?> - GIIMS</title>
    
    <script src="<?= base_url();?>assets/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
    
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">

    <link href="<?= base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/sweetalert2.min.js"></script>

    <link href="<?= base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>
    <!-- Custom styles for this template -->
    <!-- <link href="signin.css" rel="stylesheet"> -->

    <?php if(isset($css) AND array_check($css)): ?>
      <?php foreach ($css as $css_value) : ?>
        <link href="<?= base_url() . 'assets/css/' . $css_value?>" rel="stylesheet" type="text/css" />
      <?php endforeach;?>
    <?php endif;?>

    <script type="text/javascript">
      var base_url  = "<?= base_url();?>";
    </script>

  </head>

  <body class="text-center">
    <form class="form-signin" id="_form_register">
      <img src="<?= base_url();?>assets/img/Logo.png" alt="" width="200" height="200">
      <h1 class="h3 mb-3 font-weight-normal">Register</h1>
      <label class="sr-only">First Name</label>
      <input type="text" name="first_name" class="form-control" placeholder="First Name" required autofocus>
      <label class="sr-only">Middle Name</label>
      <input type="text" name="middle_name" class="form-control" placeholder="Middle Name" autofocus>
      <label class="sr-only">Last Name</label>
      <input type="text" name="last_name" class="form-control" placeholder="Last Name" required autofocus>
      <label class="sr-only">Email address</label>
      <input type="email" name="email" class="form-control" placeholder="Email address" required autofocus>
      <label class="sr-only">Password</label>
      <input type="password" name="password" class="form-control" placeholder="Password" required>   
      <label class="sr-only">Address Line 1</label>
      <input type="text" name="address_line1" name="address_line1" class="form-control" placeholder="Address Line 1" required autofocus>
      <span class="text-muted">Street address<span>
      <label class="sr-only">Address Line 2</label>
      <input type="text" name="address_line2" name="address_line2" class="form-control" placeholder="Address Line 2" required autofocus>
      <span class="text-muted">Appartment, suite, unit, building, floor, etc.<span>
      <select name="province" id="province" class="form-control" required="" autofocus>
      <option disabled="" selected="">Select Province</option>
        <?php 
          foreach($province as $row){
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }?>                         
      </select>
      <select name="city" id="city" class="form-control" required="" autofocus>
      <option disabled="" selected="">Select City</option>       
      </select>           
      <label class="sr-only">Zipcode</label>
      <input type="text" name="zipcode" id="zipcode" class="form-control" placeholder="Zipcode" required autofocus>
      <button class="btn btn-lg btn-dark btn-block" type="submit">Register</button>
      <span class="text-muted">Already have an account? <span><a href="<?= base_url()?>c">Login Now</a>
      <span id="message"><span>
    </form>
  </body>

  <?php if(isset($javascripts) AND array_check($javascripts)): ?>
    <?php foreach ($javascripts as $javascript) : ?>
      <?php if($javascript != ''):?>
        <script src="<?= base_url() . 'assets/js/' . $javascript?>"></script>
      <?php endif;?>
    <?php endforeach;?>
  <?php endif;?>

</html>
