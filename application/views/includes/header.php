<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="GIIMS">
    <meta name="author" content="kvsagun@gmail.com">
    <link rel="icon" href="<?= base_url();?>assets/img/Logo.png">

    <title><?= $nav?> - GIIMS</title>
    
    <script src="<?= base_url();?>assets/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
    
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/dashboard.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">

    <link href="<?= base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/sweetalert2.min.js"></script>

    <link href="<?= base_url();?>assets/css/froala_editor.pkgd.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/froala_editor.pkgd.min.js"></script>

    <link href="<?= base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>

    <?php if(isset($css) AND array_check($css)): ?>
      <?php foreach ($css as $css_value) : ?>
        <link href="<?= base_url() . 'assets/css/' . $css_value?>" rel="stylesheet" type="text/css" />
      <?php endforeach;?>
    <?php endif;?>

    <script type="text/javascript">
      var base_url  = "<?= base_url();?>";
    </script>

  </head>

  <body>
    <!-- <nav class="navbar navbar-expand-md navbar-dark sticky-top bg-dark">      
      <a class="avbar-brand" href="<?= base_url();?>">GIIMS</a>
      <a class="navbar-brand" href="<?= base_url();?>">
        <img src="<?= base_url();?>assets/img/Logo.png" alt="GIIMS Logo" class="img-fluid" alt="Responsive image" style="max-height: 50px;">
      </a>
      <h5 class="my-0 mr-md-auto font-weight-normal">GIIMS</h5>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="nav navbar-nav ml-auto">
          <?php
            $access = $this->session->userdata('giims_p_user_info');
           if($access['role'] == "admin"):?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url();?>main/isp">Internet Connection Type</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url();?>main/plan">Internet Plan</a>
          </li>
          <?php endif;?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url();?>main/application">Applications</a>
          </li>
          <?php if($access['role'] == "admin"):?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Customer
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="<?= base_url();?>main/a_customer">Active</a>
              <a class="dropdown-item" href="<?= base_url();?>main/b_customer">Blocklist</a>
              <a class="dropdown-item" href="<?= base_url();?>main/arc_customer">Archive</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Subscription
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="<?= base_url();?>main/a_subscription">Active</a>
              <a class="dropdown-item" href="<?= base_url();?>main/d_subscription">Disconnect</a>
              <a class="dropdown-item" href="<?= base_url();?>main/t_subscription">Terminate</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Payment
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="<?= base_url();?>main/payment_validation">For Validation</a>
              <a class="dropdown-item" href="<?= base_url();?>main/p_validated">Validated</a>
              <a class="dropdown-item" href="<?= base_url();?>main/p_reject">Rejected</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url();?>main/personnel">Personnel/Admin</a>
          </li>
          <?php endif;?>
        </ul>
        <ul class="navbar-nav px-3">
          <li class="nav-item text-nowrap">
            <a class="nav-link" href="<?= base_url() ?>c/p_logout">Sign out</a>
          </li>
        </ul>
      </div>
    </nav> -->



    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header badge-info">
            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <div class="modal-body">
              <div class="col-12">
                  <span class="confirm-message"></span>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-sm btn-primary btn-confirm-delete" id="confirm">Submit</button>
            </div>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?= base_url();?>">Gabz Internet Installation</a>
      <!-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul> -->
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <?php
                $access = $this->session->userdata('giims_p_user_info');
               if($access['role'] == "admin"):?>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/payment_method">
                  <span data-feather="credit-card"></span>
                  Payment Method
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/isp">
                  <span data-feather="server"></span>
                  Internet Connection Type
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/plan">
                  <span data-feather="shopping-cart"></span>
                  Internet Plan
                </a>
              </li>
              <?php endif;?>

              <?php if($access['role'] == "admin" || $access['role'] == "employee"):?>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/application">
                  <span data-feather="file-text"></span>
                  Applications
                </a>
              </li>
              <?php endif;?>
            </ul>

            <?php if($access['role'] == "admin"):?>
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Customer</span>
            </h6>

            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/a_customer">
                  <span data-feather="users"></span>
                  Active
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/b_customer">
                  <span data-feather="users"></span>
                  Blocklist
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/arc_customer">
                  <span data-feather="users"></span>
                  Archive
                </a>
              </li>
            </ul>
            <?php endif;?>

            <?php if($access['role'] == "admin" || $access['role'] == "employee"):?>
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Subscription</span>
            </h6>

            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/a_subscription">
                  <span data-feather="wifi"></span>
                  Active
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/d_subscription">
                  <span data-feather="wifi"></span>
                  Disconnect
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/t_subscription">
                  <span data-feather="wifi"></span>
                  Terminate
                </a>
              </li>
            </ul>
            <?php endif;?>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Payment</span>
            </h6>

            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/payment_validation">
                  <span data-feather="credit-card"></span>
                  For Validation
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/p_validated">
                  <span data-feather="credit-card"></span>
                  Validated
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/p_reject">
                  <span data-feather="credit-card"></span>
                  Rejected
                </a>
              </li>
            </ul>

            <ul class="nav flex-column mb-2">
              <?php if($access['role'] == "admin"):?>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url();?>main/personnel">
                    <span data-feather="users"></span>
                    Employee
                  </a>
                </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>main/report">
                  <span data-feather="database"></span>
                  Report
                </a>
              </li>
              <?php endif;?>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>c/p_logout">
                  <span data-feather="log-out"></span>
                  Sign out
                </a>
              </li>
            </ul>
            
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">