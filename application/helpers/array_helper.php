<?php

	if ( ! function_exists('kprint')){
		function kprint($array){
			echo '<pre>';
				print_r($array);
			echo '</pre>';
		}
	}


	if ( ! function_exists('array_check')){
		function array_check($array){
			if(is_array($array) and count($array) > 0){
				return true;
			}
			else{
				return false;
			}
		}
	}

	if ( ! function_exists('object_check')){
		function object_check($object){
			if(is_object($object) and count($object) > 0){
				return true;
			}
			else{
				return false;
			}
		}
	}

	if ( ! function_exists('ellipse_file_name')){
		function ellipse_file_name($filename){

			if($filename){

			}

			return $filename;
		}
	}


if ( ! function_exists('new_csv_to_array')){
	function new_csv_to_array($file, $is_import = FALSE, $delimiter=",",$is_time_logs =FALSE){
		//requires csv file
		// returns array of the csv
		$csv_array = array();

		if( file_exists(str_replace(base_url(),'',$file)) ){

			//we make the url absolute path of the file
			$file = str_replace(base_url(),'',$file);
			$basepath = str_replace('\\system\\','\\', BASEPATH);
			$file = $basepath.$file;

			if (($handle = fopen($file, "r")) !== FALSE) {

					if($is_import === TRUE){
						//line length was originally set to 1000
						while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {

								$csv_array[] = $data;

						}
					}
					else if($is_time_logs == TRUE){


						$data = file_get_contents($file).'';
						$data = str_replace("\t",',',$data);

						$datas = explode("\n", $data);

						foreach ($datas as $key => $value) {
							$data_arr =  new_str_getcsv($value,',');
							$csv_array[] = $data_arr;

						}

					}
					else{


						$contents = '';

						while ( ($buf=fread( $handle, 8192 )) != '' ) {
								// Here, $buf is guaranted to contain data
								$contents .= $buf;
						}
						if($buf===FALSE) {
								echo "THERE WAS AN ERROR READING THE FILE\n"; exit;
						}

						//$contents = fread($handle, filesize(str_replace(base_url(),'',$file)) * 2);
						//$contents = str_replace("\n","\r\n",$contents);
						//$pre_contents = substr($contents,0,500);

						//$contents = str_replace($pre_contents,'',$contents);

						//$pre_contents = new_nl2br2($pre_contents);
						//$pre_contents = str_replace("<br />","\n",$pre_contents);
						//$contents = $pre_contents . $contents;
						$contents = nl2br($contents);
						$contents = str_replace("<br />","\n",$contents);

						$data = explode("\n",$contents);

						if(count($data)>0){
							for($x=0; $x<=count($data)-1; $x++){
								$line = trim($data[$x]);
								//$line_explode = explode(",",$line);
								$col = new_str_getcsv($data[$x], $delimiter);
								$csv_array[] = $col;
							}
						}

					}

					fclose($handle);
			}
			return $csv_array;
		}
		else{
			return FALSE;
		}

	}


	}

if (!function_exists('new_str_getcsv')) {
	function new_str_getcsv($input, $delimiter = ",", $enclosure = '"', $escape = "\\",$import_time_logs = false) {
		$fiveMBs = 10 * 1024 * 1024;
		$fp = fopen("php://temp/maxmemory:$fiveMBs", 'r+');
		fputs($fp, $input);
		rewind($fp);

		$data = fgetcsv($fp, 0, $delimiter, $enclosure); //  $escape only got added in 5.3.0

		fclose($fp);

		return $data;
	}
}

?>
